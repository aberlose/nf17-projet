Résumé des classes du MCD (corrigé) complétées avec le type des attributs et les différentes contraintes.

- **Membre**

Numero membre : integer (key)

Nom : string (not null)

Prenom : string (not null)

DdN : date (not null)

Adresse : string (not null)

Tel : integer(10) (not null)

Etat du dossier : boolean (not null)

- **Responsabilite**

Responsabilite membre : {president, membre} (not null)

- **Inscription**

Numero inscription : integer (key)

Annee inscription : integer(4) (not null)

Status membre : {amateur, professionnel} (not null)

Frais inscription : {0,25} (not null)

- **Resultat**

Numero resultat : integer (key)

Date resultat : date (not null)

Rang : integer (not null and >=0)

Points : integer (not null and >=0)

- **Specialite**

Numero specialite : integer (key)

Nom specialite : string (not null)

- **Activite**

Numero activite : integer (key)

Titre activite : string (not null)

Type activite : {financement, competition} (not null)

Adresse activite : string (not null)

- **Paiement**

Numero paiement : integer (key)

Montant paiement : float (not null)

- **Discipline**

Numero discipline : integer (key)

Nom discipline : string (not null)

- **Financement**

Date activite : date (not null)

Heure activite : time (not null)

- **Compétition**

Date debut : date (not null)

Date fin : date (not null)

Lieu competition : string (not null)

- **CompetitionInterne**

- **CompetitionExterne**

- **Epreuve**

Numero epreuve : integer (key)

Date epreuve : date (not null)

Heure epreuve : time (not null)

- **OrganisateurExterne**

Numero organisateur : integer (key)

Nom organisateur : string (not null)

Adresse : string (not null)

NomResponsable : string (not null)

PrenomResponsable : string (not null)

TelResponsable : integer(10) (not null)