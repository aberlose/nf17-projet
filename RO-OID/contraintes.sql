-- On s'assure qu'il y a exactement 3 membres par équipe

CREATE VIEW vEquipes AS -- On sélectionne toutes les équipes
SELECT *
FROM TABLE(Competition.equipes) e; 

CREATE VIEW vMembresEquipes AS -- Si la table est vide, alors on a bien 3 membres exactement par équipe
SELECT e.nom_equipe, COUNT(m.membre_numero)
FROM vEquipes e, TABLE(e.membres_equipes) m
GROUP BY e.nom_equipe
HAVING (COUNT(m.membre_numero)>3) OR (COUNT(m.membre_numero)<3)

-- On s'assure qu'il y a au moins 2 équipes par compétition (sinon la compétition n'a pas lieu d'être)

CREATE VIEW vNbEquipes AS -- Si la table est vide alors au moins 2 équipes participent à chaque compétition
SELECT activite_numero, COUNT(e.nom_equipe)
FROM Competition c, TABLE(c.equipes) e
GROUP BY activite_numero
HAVING COUNT(e.nom_equipe)<2;
