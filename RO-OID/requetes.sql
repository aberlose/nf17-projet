SELECT m.nom, m.prenom
FROM Membre m, EstSpecialise ES, Specialite S
WHERE S.spe_nom = 'Brasse'
AND m.membre_numero = ES.membre_numero AND  S.spe_numero = ES.spe_numero
GROUP BY  m.nom, m.prenom;

SELECT c.equipes.membres_equipes.membre_numero.membre_nom AS Nom, c.equipes.membres_equipes.membre_numero.membre_prenom AS Prenom
FROM Competition c
WHERE c.activite_numero.nom_activite = 'Activite du Lundi'
GROUP BY c.equipes.membres_equipes.membre_numero.membre_nom, c.equipes.membres_equipes.membre_numero.membre_prenom;

SELECT EXTRACT(HOUR from CAST(to_char(E.epreuve_date,'DD/MM/YYYY HH24:MI:SS') AS TIMESTAMP)) AS Date, E.epreuve_time AS Heure
FROM Epreuve E
WHERE E.activite_numero.nom_activite = 'Activite du Lundi'
GROUP BY E.epreuve_date, E.epreuve_time
HAVING '12:00:00' < EXTRACT(HOUR from CAST(to_char(E.epreuve_date,'DD/MM/YYYY HH24:MI:SS') AS TIMESTAMP)) AND EXTRACT(HOUR from CAST(to_char(E.epreuve_date,'DD/MM/YYYY HH24:MI:SS') AS TIMESTAMP)) <'18:00:00';
