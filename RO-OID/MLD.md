Type tAdresse : <num_rue : number, nom_voie : string, ville : string>

Type tTel : <domicile : number(10), bureau : number(10)>

Type tMembre : <membre_numero : integer, nom : string, prenom : string, ddN : date, adresse : tAdresse, tel : tTel, etat_dossier : boolean>

Membre de tMembre (#membre_numero) avec nom, prenom, DdN, adresse, tel, etat_dossier NOT NULL


Type tActivite : <activite_numero : integer, activite_titre : string, activite_type : {financement, competition}, activite_adresse : string>

Activite de tActivite (#activite_numero) avec activite_titre, activite_type, activite_adresse NOT NULL


Type tResponsabilite : <membre_numero=>o Membre, activite_numero=>o Activite, resp_membre : {president, membre} >

Responsabilite de tResponsabilite avec resp_membre, membre_numero, activite_numero NOT NULL


Type tEpreuve : <epreuve_numero : integer, activite_numero=>o Activite, epreuve_heure : time, spe_numero=>o Specialite >

Epreuve de tEpreuve (#epreuve_numero) avec epreuve_heure, spe_numero, activite_numero NOT NULL


Type tResultat : <membre_numero=>o Membre, epreuve_numero=>o Epreuve, result_numero : integer, rang : integer, points : integer>

Resultat de tResultat(#result_numero) avec rang, points, membre_numero, epreuve_numero NOT NULL


Type tInscription : <insc_numero : integer, insc_annee : integer(4), status_membre : {am, pro}, insc_frais : {0,25}, membre_numero=>o Membre> 

Inscription de tInscription(#insc_numero) avec insc_annee, status_membre, insc_frais, membre_numero NOT NULL


Type tPaiement :  <paiement_numero : integer, paiement_montant : float, insc_numero=>o Inscription>

Paiement de tPaiement(#paiement_numero) avec paiement_montant, insc_numero NOT NULL


Type tSpecialite : <spe_numero : integer, spe_nom : string, discipline_numero=>o Discipline>

Specialite de tSpecialite (#spe_numero) avec spe_nom et discipline_numero NOT NULL


EstSpecialise(#membre_numero=>Membre, #spe_numero=>Specialite)


Type tDiscipline : <discipline_numero : integer, discipline_nom : string>

Discipline de tDiscipline(#discipline_numero) avec discipline_nom NOT NULL


Type tFinancement : <activite_numero =>o Activite, la_date : date, heure : time> 

Financement de tFinancement (pas de clé ici) avec la_date, heure, activite_numero NOT NULL 


Type tNom : <nom_membre : string, prenom_membre : string>

Type col_nom : collection de tNom

Type tEquipe : <nom_equipe : string, membres_equipe : col_nom> avec 3 éléments dans "membres"

Type col_equipe : collection de tEquipe

Type tCompetition : <activite_numero =>o Activite, date_debut : date, date_fin : date, lieu : string, equipes : col_equipe>

Competition de tCompetition avec date_debut, date_fin, lieu, activite_numero NOT NULL, equipes >= 2

CompetitionInterne(activite_numero =>o Activite)

CompetitionExterne (activite_numero=>o Activité, orga_numero=>o OrganisateurExterne) avec orga_numero NOT NULL


Type tOrganisateurExterne : <orga_numero : integer, orga_nom : string, orga_adresse : tAdresse, nom_responsable : string, prenom_responsable : string, tel_responsable : integer(10)>

OrganisateurExterne de tOrganisateurExterne(#orga_numero) avec orga_nom, orga_adresse, nom_responsable, prenom_responsable, tel_responsable NOT NULL

Projection(Activite,numeroActivite) IN Union(Projection(Financement,numeroActivite),Projection(Competition,numeroActivite)) Intersection(Projection(Financement,numeroActivite),Projection(Competition,numeroActivite)) IS NULL
