DECLARE 
refMembre REF tMembre;
refInscription REF tInscription;
refActivite REF tActivite;
refMembre2 REF tMembre;
refMembre3 REF tMembre;
refMembre4 REF tMembre;
refMembre5 REF tMembre;
refMembre6 REF tMembre;
refDiscipline1 REF tDiscipline;
refDiscipline2 REF tDiscipline;
refSpecialite4 REF tSpecialite;
refEpreuve REF tEpreuve;

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (1,'Dupont','Bertrand',TO_DATE('01/01/1951','DD/MM/YYYY'),tAdresse(33,'rue du Chene','Paris'), tTel(0171285513,0612344567), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(1, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 1;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(1,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (7,'Winehouse','Amy',TO_DATE('04/06/1981','DD/MM/YYYY'),tAdresse(22,'rue du Gros Sapin','Paris'), tTel(0144283745,0692314573), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 7;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(7, 2019, 'pro', 25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 7;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(7,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (3,'Sapristi','Poulet',TO_DATE('03/02/2000','DD/MM/YYYY'),tAdresse(2,'rue du Paradis','Compiegne'), tTel(0172345513,0612383467), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 3;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(3, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 3;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(3,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (4,'Jeanne','Tropbelle',TO_DATE('10/11/1971','DD/MM/YYYY'),tAdresse(6,'rue du Rien','Paris'), tTel(0170000513,0612256567), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 4;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(4, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 4;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(4,25,refInscription);

END;
/
BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (5,'Marine','Blonde',TO_DATE('24/12/1981','DD/MM/YYYY'),tAdresse(8,'rue du Pouloulou','Paris'), tTel(0171251213,0656744567), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 5;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(5, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 5;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(5,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (6,'Mauve','Berthe',TO_DATE('23/04/1989','DD/MM/YYYY'),tAdresse(12,'rue des Lilas','Mulhouse'), tTel(0345612366,0644257299), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Inscription (insc_numero, insc_annee, status_membre, insc_frais, membre_numero)
VALUES(6, 2019, 'pro', 25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 6;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(6, 25, refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (2,'Aubry','Salome',TO_DATE('01/06/1998','DD/MM/YYYY'),tAdresse(5,'rue du General Koenig','Paris'), tTel(0171286789,0612583567), 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 2;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(2, 2019, 'am' ,0, refMembre);
END;
/

BEGIN 

INSERT INTO Activite (activite_numero,activite_titre, activite_type, activite_adresse)
VALUES (1,'Activite du Lundi','competition',tAdresse(4,'rue du Clos des Roses', 'Compiegne');

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Competition (activite_numero,date_debut,date_fin,lieu,equipes)
VALUES(refActivite,TO_DATE('18/04/2019','DD/MM/YYYY'), TO_DATE('20/04/2019','DD/MM/YYYY'), 'Paris', col_equipe('Les Grands', col_membres(refMembre, refMembre1, refMembre2), col_membres(refMembre3, refMembre4, refMembre5));

INSERT INTO CompetitionInterne (activite_numero)
VALUES(1);

END;
/

BEGIN

INSERT INTO Discipline(discipline_numero,discipline_nom)
VALUES(1, 'Natation');

INSERT INTO Discipline(discipline_numero,discipline_nom)
VALUES(2, 'Cyclisme');

SELECT REF(d) INTO refDiscipline1
FROM Discipline d
WHERE discipline_numero = 1;

SELECT REF(d) INTO refDiscipline2
FROM Discipline d
WHERE discipline_numero = 2;

INSERT INTO Specialite(spe_numero, spe_nom, discipline_numero)
VALUES(1, 'Criterium', refDiscipline1);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(2, 'Brasse', refDiscipline2);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(3, '100 m', refDiscipline1);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(4, 'Contre la montre', refDiscipline2);

END;
/

INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(1,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(3,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(4,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(5,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(6,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(7,4);

BEGIN

SELECT REF(s) INTO refSpecialite4
FROM Specialite s 
WHERE spe_numero = 4;

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

INSERT INTO Epreuve (epreuve_numero, activite_numero, horaire, spe_numero)
VALUES(1,refActivite,TO_DATE('18/04/2019 15:00:00','DD/MM/YYYY HH24:MI:SS'), refSpecialite4);

END;
/

BEGIN

SELECT REF(e) INTO refEpreuve
FROM Epreuve e 
WHERE epreuve_numero = 1;

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre,refEpreuve,1,6,10);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre2,refEpreuve,2,4,40);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre3,refEpreuve,3,2,80);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre4,refEpreuve,4,1,100);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre5,refEpreuve,4,5,20);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre6,refEpreuve,5,3,60);

END;
/

BEGIN

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre, refActivite, 'president');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre2, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre3, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre4, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre5, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre6, refActivite, 'membre');

END;

/

