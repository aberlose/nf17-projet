-- On s'assure qu'il y a exactement 5 membres par équipe

CREATE VIEW vEquipes AS -- On sélectionne toutes les équipes
SELECT *
FROM TABLE(Competition.equipes) e; 

CREATE VIEW vMembresEquipes AS -- Si la table est vide, alors on a bien 5 membres exactement par équipe
SELECT e.nom_equipe, COUNT(m.nom_membre)
FROM vEquipes e, TABLE(e.membres_equipes) m
GROUP BY e.nom_equipe
HAVING (COUNT(m.nom_membre)>5) OR (COUNT(m.nom_membre)<5)

-- On s'assure qu'il y a au moins 2 équipes par compétition (sinon la compétition n'a pas lieu d'être)

CREATE VIEW vNbEquipes AS -- Si la table est vide alors au moins 2 équipes participent à chaque compétition
SELECT numeroActivite, COUNT(e.nom_equipe)
FROM Competition c, TABLE(c.equipes) e
GROUP BY numeroActivite
HAVING COUNT(e.nom_equipe)<2;

