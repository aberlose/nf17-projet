CREATE OR REPLACE TYPE typ_tel AS OBJECT (
domicile NUMBER(10),
bureau NUMBER(10)
);

CREATE TABLE Membre (
numeroMembre INTEGER,
nom VARCHAR NOT NULL,
prenom VARCHAR NOT NULL,
dateDeNaissance DATE NOT NULL,
numero_adresse INTEGER,
rue VARCHAR,
ville VARCHAR,
telephone typ_tel,
etatDossier NUMERIC(1),
PRIMARY KEY (numeroMembre)
);

CREATE TABLE Activite (
numeroActivite INTEGER,
titreActivite VARCHAR NOT NULL,
adresseActivite VARCHAR,
PRIMARY KEY (numeroActivite)
);

CREATE OR REPLACE TYPE typ_nom AS OBJECT (
nom_membre VARCHAR,
prenom_membre VARCHAR
);

CREATE TYPE col_nom AS TABLE OF typ_nom;

CREATE OR REPLACE TYPE typ_equipe AS OBJECT (
nom_equipe VARCHAR,
membres_equipes col_nom
)
NESTED TABLE membres_equipes STORE AS membres;

CREATE TYPE col_equipe AS TABLE OF typ_equipe;

CREATE TABLE Competition (
numeroActivite INTEGER,
dateDebut DATE,
dateFin DATE,
lieuCompetition VARCHAR,
equipes col_equipe,
PRIMARY KEY (numeroActivite),
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite)
)
NESTED TABLE equipes STORE AS compet_equipes;


CREATE TABLE Financement (
numeroActivite INTEGER,
dateActivite DATE,
heureActivite TIME,
PRIMARY KEY (numeroActivite),
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite)
);

CREATE VIEW vCompetition
AS
SELECT A.numeroActivite
FROM Activite A JOIN Competition C ON A.numeroActivite=C.numeroActivite;

CREATE VIEW vFinancement
AS
SELECT A.numeroActivite
FROM Activite A JOIN Financement F ON A.numeroActivite=F.numeroActivite;

CREATE TABLE CompetitionInterne (
numeroActivite INTEGER,
titre VARCHAR,
PRIMARY KEY (numeroActivite),
FOREIGN KEY (numeroActivite) REFERENCES Activite
);

CREATE TABLE OrganisateurExterne (
numeroOrganisateur INTEGER PRIMARY KEY,
nomOrganisateur VARCHAR NOT NULL,
numAdresseOrganisateur INTEGER,
rueOrganisateur VARCHAR,
villeOrganisateur VARCHAR,
nomResponsable VARCHAR NOT NULL,
prenomResponsable VARCHAR NOT NULL,
telephoneResponsable NUMERIC(10) NOT NULL
);

CREATE TABLE CompetitionExterne (
numeroActivite INTEGER,
organisateurExterne INTEGER NOT NULL,
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite),
FOREIGN KEY (organisateurExterne) REFERENCES OrganisateurExterne(numeroOrganisateur)
);

CREATE VIEW vCompetitionInterne
AS
SELECT C.numeroActivite
FROM Competition C JOIN CompetitionInterne CI ON C.numeroActivite=CI.numeroActivite;

CREATE VIEW vCompetitionExterne
AS
SELECT C.numeroActivite
FROM Competition C JOIN CompetitionExterne CE ON C.numeroActivite=CE.numeroActivite;

CREATE TABLE Discipline (
numeroDiscipline INTEGER PRIMARY KEY,
nomDiscipline VARCHAR NOT NULL
) ;

CREATE TABLE Specialite (
numeroSpecialite INTEGER,
nomSpecialite VARCHAR NOT NULL,
discipline INTEGER NOT NULL,
PRIMARY KEY (numeroSpecialite),
FOREIGN KEY (discipline) REFERENCES Discipline(numeroDiscipline)
);

CREATE TABLE Epreuve (
competition INTEGER,
numeroEpreuve INTEGER,
dateEpreuve DATE NOT NULL,
heureEpreuve TIME,
specialite INTEGER NOT NULL,
PRIMARY KEY (competition, numeroEpreuve),
FOREIGN KEY (competition) REFERENCES Competition(numeroActivite),
FOREIGN KEY (specialite) REFERENCES Specialite(numeroSpecialite)
);

CREATE TABLE Resultat (
numeroResultat INTEGER,
membre INTEGER,
numeroEpreuve INTEGER,
competitionEpreuve INTEGER,
rang INTEGER NOT NULL,
points INTEGER,
PRIMARY KEY (numeroResultat, membre, numeroEpreuve, competitionEpreuve),
FOREIGN KEY (membre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (numeroEpreuve, competitionEpreuve) REFERENCES Epreuve(numeroEpreuve, competition)
);

CREATE TABLE FraisInscription (
statutMembre VARCHAR NOT NULL,
fraisInscription FLOAT NOT NULL,
PRIMARY KEY (statutMembre)
);

CREATE TABLE Inscription (
numeroInscription INTEGER,
anneeInscription NUMERIC(4) NOT NULL,
membre INTEGER NOT NULL,
statutMembre VARCHAR NOT NULL,
PRIMARY KEY (numeroInscription),
FOREIGN KEY (membre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (statutMembre) REFERENCES FraisInscription(statutMembre)
);

CREATE TABLE Paiement (
numeroPaiement INTEGER PRIMARY KEY,
montantPaiement INTEGER NOT NULL,
inscription INTEGER NOT NULL,
FOREIGN KEY (inscription) REFERENCES Inscription(numeroInscription)
);

CREATE TABLE Responsabilite (
numeroMembre INTEGER,
numeroActivite INTEGER,
responsabiliteMembre VARCHAR,
PRIMARY KEY (numeroMembre, numeroActivite, responsabiliteMembre),
FOREIGN KEY (numeroMembre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite)
);

CREATE TABLE Specialisation (
membre INTEGER,
specialite INTEGER,
PRIMARY KEY (membre, specialite),
FOREIGN KEY (membre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (specialite) REFERENCES Specialite(numeroSpecialite)
);
