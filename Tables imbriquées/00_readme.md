# Étape de projet n°9 : Tables imbriquées

### Consignes

- Dans la classe Membre, redéfinir l'attribut téléphone. Dans cet attribut on met le téléphone du bureau et du domicile (peuvent être nuls). 
- Dans Competition, ajouter une composition vers Équipes (chaque compétition est composées d’équipes). Dans la table Équipe, il faut le nom de l’équipe et le nom des 5 membres qui la composent.
- Regarder si on peut faire Épreuve en RO (expliquer pourquoi ça marche/pourquoi ça marche pas). Est-ce que c’est possible de le représenter (Épreuve) en tableau imbriqué ?

Réponse : On se demande s'il est possible de représenter Épreuve en tableau imbriqué. Cela n'est pas possible car Épreuve est associé à la fois à Compétition, à Spécialité et à Membre. Il faudrait donc imbriquer Épreuve dans ces 3 classes ce qui entraînerait des répétitions. 

- Faire des INSERT pour tester



### Fichiers rendus

Les fichiers rendus sont les fichiers rendus à l'étape LDD mais complétés.

- *MLD.md* : modification du MLD initial
- *createTable.sql* : modification de la classe Membre et de la classe Competition
- *contraintes.sql* : nouvelles contraintes pour vérifier que les équipes sont bien constituées de 5 membres et pour vérifier qu'il y a au moins 2 équipes par compétition.
- *insert.sql*