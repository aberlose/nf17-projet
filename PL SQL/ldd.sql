CREATE TABLE Membre (
numeroMembre NUMBER,
nom VARCHAR2(50) NOT NULL,
prenom VARCHAR2(50) NOT NULL,
dateDeNaissan VARCHAR2(50ce DATE NOT NULL,
numero_adresse NUMBER,
rue VARCHAR2(50),
ville VARCHAR2(50),
telephone NUMBER(10),
etatDossier NUMBER(1),
PRIMARY KEY (numeroMembre)
);

CREATE TABLE Activite (
numeroActivite NUMBER,
titreActivite VARCHAR2(50) NOT NULL,
adresseActivite VARCHAR2(50),
PRIMARY KEY (numeroActivite)
);

CREATE TABLE Competition (
numeroActivite NUMBER,
dateDebut DATE,
dateFin DATE,
lieuCompetition VARCHAR2(50),
PRIMARY KEY (numeroActivite),
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite)
);

CREATE TABLE Financement (
numeroActivite NUMBER,
dateActivite DATE,
heureActivite TIME,
PRIMARY KEY (numeroActivite),
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite)
);

CREATE VIEW vCompetition
AS
SELECT A.numeroActivite
FROM Activite A JOIN Competition C ON A.numeroActivite=C.numeroActivite;

CREATE VIEW vFinancement
AS
SELECT A.numeroActivite
FROM Activite A JOIN Financement F ON A.numeroActivite=F.numeroActivite;

CREATE TABLE CompetitionInterne (
numeroActivite NUMBER,
titre VARCHAR2(50),
PRIMARY KEY (numeroActivite),
FOREIGN KEY (numeroActivite) REFERENCES Activite
);

CREATE TABLE OrganisateurExterne (
numeroOrganisateur NUMBER PRIMARY KEY,
nomOrganisateur VARCHAR2(50) NOT NULL,
numAdresseOrganisateur NUMBER,
rueOrganisateur VARCHAR2(50),
villeOrganisateur VARCHAR2(50),
nomResponsable VARCHAR2(50) NOT NULL,
prenomResponsable VARCHAR2(50) NOT NULL,
telephoneResponsable NUMBER(10) NOT NULL
);

CREATE TABLE CompetitionExterne (
numeroActivite NUMBER,
organisateurExterne NUMBER NOT NULL,
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite),
FOREIGN KEY (organisateurExterne) REFERENCES OrganisateurExterne(numeroOrganisateur)
);

CREATE VIEW vCompetitionInterne
AS
SELECT C.numeroActivite
FROM Competition C JOIN CompetitionInterne CI ON C.numeroActivite=CI.numeroActivite;

CREATE VIEW vCompetitionExterne
AS
SELECT C.numeroActivite
FROM Competition C JOIN CompetitionExterne CE ON C.numeroActivite=CE.numeroActivite;

CREATE TABLE Discipline (
numeroDiscipline NUMBER PRIMARY KEY,
nomDiscipline VARCHAR2(50) NOT NULL
) ;

CREATE TABLE Specialite (
numeroSpecialite NUMBER,
nomSpecialite VARCHAR2(50) NOT NULL,
discipline NUMBER NOT NULL,
PRIMARY KEY (numeroSpecialite),
FOREIGN KEY (discipline) REFERENCES Discipline(numeroDiscipline)
);

CREATE TABLE Epreuve (
competition NUMBER,
numeroEpreuve NUMBER,
dateEpreuve DATE NOT NULL,
heureEpreuve TIME,
specialite NUMBER NOT NULL,
PRIMARY KEY (competition, numeroEpreuve),
FOREIGN KEY (competition) REFERENCES Competition(numeroActivite),
FOREIGN KEY (specialite) REFERENCES Specialite(numeroSpecialite)
);

CREATE TABLE Resultat (
numeroResultat NUMBER,
membre NUMBER,
numeroEpreuve NUMBER,
competitionEpreuve NUMBER,
rang NUMBER NOT NULL,
points NUMBER,
PRIMARY KEY (numeroResultat, membre, numeroEpreuve, competitionEpreuve),
FOREIGN KEY (membre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (numeroEpreuve, competitionEpreuve) REFERENCES Epreuve(numeroEpreuve, competition)
);

CREATE TABLE FraisInscription (
statutMembre VARCHAR2(50) NOT NULL,
fraisInscription NUMBER NOT NULL,
PRIMARY KEY (statutMembre)
);

CREATE TABLE Inscription (
numeroInscription NUMBER,
anneeInscriNUMBERption NUMBER(4) NOT NULL,
membre NUMBER NOT NULL,
statutMembre VARCHAR2(50) NOT NULL,
PRIMARY KEY (numeroInscription),
FOREIGN KEY (membre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (statutMembre) REFERENCES FraisInscription(statutMembre)
);

CREATE TABLE Paiement (
numeroPaiement NUMBER PRIMARY KEY,
montantPaiement NUMBER NOT NULL,
inscription NUMBER NOT NULL,
FOREIGN KEY (inscription) REFERENCES Inscription(numeroInscription)
);

CREATE TABLE Responsabilite (
numeroMembre NUMBER,
numeroActivite NUMBER,
responsabiliteMembre VARCHAR2(50),
PRIMARY KEY (numeroMembre, numeroActivite, responsabiliteMembre),
FOREIGN KEY (numeroMembre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (numeroActivite) REFERENCES Activite(numeroActivite)
);

CREATE TABLE Specialisation (
membre NUMBER,
specialite NUMBER,
PRIMARY KEY (membre, specialite),
FOREIGN KEY (membre) REFERENCES Membre(numeroMembre),
FOREIGN KEY (specialite) REFERENCES Specialite(numeroSpecialite)
);
