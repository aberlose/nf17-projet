\set AUTOCOMMIT on

INSERT INTO FraisInscription (statutMembre,fraisInscription) VALUES ('Professionnel',25);

INSERT INTO FraisInscription (statutMembre,fraisInscription) VALUES ('Amateur', 0);

/*Pour qu'un membre soit totalement inscrit, il faut que l'ensemble des informations des relations
Membre, Inscription et Paiement soient renseignées. On utilise donc une transaction pour encapsuler les 3 INSERT nécessaires
à l'inscription complète d'un membre (INSERT INTO Membre, INSERT INTO Inscription, INSERT INTO Paiement)*/

BEGIN TRANSACTION;
INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (1,'Dupont','Bertrand',TO_DATE('01/01/1951','DD/MM/YYYY'),33,'rue du Chene','Paris', 0612344567, 1);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(1, 2017, 1,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(1,25,1);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (2,'Pouillet','Salome',TO_DATE('29/09/1998','DD/MM/YYYY'),2,'rue du Rosier','Paris', 0696344567, 0);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(2, 2017, 2,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(2,5,2);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (3,'Auclour','Mathilde',TO_DATE('14/03/1997','DD/MM/YYYY'),6,'rue du Chevreuil','Paris', 0618544567, 1);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(3, 2017, 3,'Amateur');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(3,25,3);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (4,'Clarke','Spencer',TO_DATE('05/06/1989','DD/MM/YYYY'),12,'avenue de la Libération','Toulouse', 0664368902, 1);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(4, 2018, 4,'Amateur');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(4,0,4);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (5,'Huffman','Gabriella',TO_DATE('27/11/1976','DD/MM/YYYY'),68,'boulevard des États-Unis','Nantes', 0789451344, 1);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(5, 2018, 5,'Amateur');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(5,0,5);
COMMIT TRANSACTION;

/* Un membre se réinscrit : on saisit donc une nouvelle inscription et un nouveau paiement */
BEGIN TRANSACTION;
INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(6, 2019, 4,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(6,25,6);
COMMIT TRANSACTION;

/* Pour qu'une activité soit rentrée correctement dans la base de données, on doit obligatoirement saisir les informations concernant
l'activité elle-même (table ACTIVITE) mais aussi les informations de la table COMPETITION ou bien FINANCEMENT. De plus, si c'est
une compétition, on doit saisir les informations dans CompetitionInterne ou bien dans CompetitionExterne. Enfin, s'il s'agit d'une
compétition externe, l'ajout de l'activité ne sera complet qu'après avoir renseigné les informations concernant l'organisateur.
On utilise donc des transactions pour réaliser les différents INSERT pour l'ajout des activités.*/

BEGIN TRANSACTION;
INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (1,'Activite du Lundi','Clos des Roses');

INSERT INTO Competition (numeroActivite,dateDebut,dateFin, lieuCompetition)
VALUES(1,TO_DATE('18/04/2019','DD/MM/YYYY'), TO_DATE('20/04/2019','DD/MM/YYYY'), 'Paris');

INSERT INTO CompetitionInterne (numeroActivite,titre)
VALUES(1, 'Grande Compet');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (2,'Activite du Mercredi','Mercieres');

INSERT INTO Competition (numeroActivite,dateDebut,dateFin, lieuCompetition)
VALUES(2,TO_DATE('30/03/2019','DD/MM/YYYY'), TO_DATE('03/04/2019','DD/MM/YYYY'), 'Lyon');

INSERT INTO OrganisateurExterne(numeroOrganisateur, nomOrganisateur,numAdresseOrganisateur,rueOrganisateur,villeOrganisateur,nomResponsable,prenomResponsable,telephoneResponsable)
VALUES(1, 'Club des Sportifs',18,'rue du Chemin Vert','Compiegne', 'Benoit', 'Bastien', '0621887068');

INSERT INTO CompetitionExterne (numeroActivite,organisateurExterne)
VALUES(2, 1);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (3,'Activite du Jeudi','Halles des Sports');

INSERT INTO Financement (numeroActivite,dateActivite,heureActivite)
VALUES(3,TO_DATE('15/09/2019','DD/MM/YYYY'), '15:12:54');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (4,'Championnats du monde de Natation','Moscou');

INSERT INTO Competition (numeroActivite,dateDebut,dateFin, lieuCompetition)
VALUES(4,TO_DATE('04/03/2018','DD/MM/YYYY'), TO_DATE('21/03/2018','DD/MM/YYYY'), 'Piscine Olympique de Moscou');

INSERT INTO OrganisateurExterne(numeroOrganisateur, nomOrganisateur,numAdresseOrganisateur,rueOrganisateur,villeOrganisateur,nomResponsable,prenomResponsable,telephoneResponsable)
VALUES(2, 'Club des nageurs russes',1465,'avenue du Kremlin','Moscou', 'Sharapolov', 'Grigor', '0688349074');

INSERT INTO CompetitionExterne (numeroActivite,organisateurExterne)
VALUES(4,2);
COMMIT TRANSACTION;

/* Pour tous les autres INSERT, on utilise le mode autocommit (déclaré comme "on" au début du fichier) :
chaque instruction est donc encapsulée dans une transaction.*/

INSERT INTO Discipline(numeroDiscipline,nomDiscipline)
VALUES(1, 'Marche à pied');

INSERT INTO Discipline(numeroDiscipline,nomDiscipline)
VALUES(2, 'Football');

INSERT INTO Discipline(numeroDiscipline,nomDiscipline)
VALUES(3, 'Natation');

INSERT INTO Discipline(numeroDiscipline,nomDiscipline)
VALUES(4, 'Cyclisme');

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(1, 'Nage Libre', 3);

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(2, 'Foot en Salle', 2);

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(3, '100 m', 1);

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(4, 'Brasse', 3);

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(5, '100 m', 3);

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(6, 'Contre la montre', 4);

INSERT INTO Epreuve (competition,numeroEpreuve,dateEpreuve,heureEpreuve,specialite)
VALUES(1,1,TO_DATE('18/04/2019','DD/MM/YYYY'),'15:00:00', 4);

INSERT INTO Epreuve (competition,numeroEpreuve,dateEpreuve,heureEpreuve,specialite)
VALUES(2,2,TO_DATE('31/03/2019','DD/MM/YYYY'),'15:00:00', 3);

INSERT INTO Epreuve (competition,numeroEpreuve,dateEpreuve,heureEpreuve,specialite)
VALUES(4,3,TO_DATE('6/03/2018','DD/MM/YYYY'),'14:30:00', 3);

INSERT INTO Resultat (numeroResultat,membre,numeroEpreuve,competitionEpreuve,rang,points)
VALUES(1,1,1,1,2,45);

INSERT INTO Resultat (numeroResultat,membre,numeroEpreuve,competitionEpreuve,rang,points)
VALUES(2,2,2,2,1,18);

INSERT INTO Resultat (numeroResultat,membre,numeroEpreuve,competitionEpreuve,rang,points)
VALUES(3,4,3,4,2,477);

INSERT INTO Responsabilite (numeroMembre,numeroActivite,responsabiliteMembre)
VALUES(1, 1, 'Membre');

INSERT INTO Responsabilite (numeroMembre,numeroActivite,responsabiliteMembre)
VALUES(2, 2, 'Membre');

INSERT INTO Specialisation (membre,specialite)
VALUES(1, 3);

INSERT INTO Specialisation (membre,specialite)
VALUES(2, 2);

INSERT INTO Specialisation (membre,specialite)
VALUES(4, 5);
