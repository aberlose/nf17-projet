# Preuve de normalisation

## Rappels

- On peut définir une **DF** X→Y si pour une valeur de X, on a toujours la même valeur de Y (on dit que X détermine Y).
- Une **clé** est donc un ensemble minimum d'attributs d'une relation qui détermine tous les autres. Si une relation comporte plusieurs clés, chacune est dite clé candidate et l'on en choisit une en particulier pour être la clé primaire. 

- ***Première forme normale (1NF)*** : une relation est en 1NF si elle possède au moins une clé et si tous ses attributs sont atomiques.

- ***Deuxième forme normale (2NF)*** : une relation est en 2NF si elle est en 1NF et si tout attribut n'appartenant à aucune clé candidate ne dépend pas d'une partie seulement de cette clé candidate. 

- ***Troisième forme normale (3NF)*** : une relation est en 3NF si elle est en 2NF et si tout attribut n'appartenant à aucune clé candidate ne dépend directement que de clés candidates.

- ***Forme normale de Boyce-Codd*** ***(BCNF)*** : une relation est en BCNF si elle est en 3NF et si les seules DFE existantes sont celles pour lesquelles une clé candidate détermine un attribut (toutes les DFE sont issues d'une clé).

## Introduction

Pour chaque relation de notre base de données, nous présentons l'ensemble des dépendances fonctionnelles, les clés candidates et choisissons une clé primaire si nécessaire. Nous cherchons ensuite à vérifier que la relation est en BCNF (en passant par les étapes 1NF, 2NF et 3NF). Si une relation n'est pas en BCNF, nous effectuons les changements nécessaires pour n'obtenir que des tables en BCNF.

## Inscription

```
Inscription (
    #insc_numero : integer, 
    insc_annee : integer(4), 
    status_membre : {am, pro}, 
    insc_frais : {0,25}, 
    membre_numero=>Membre
) avec insc_annee, status_membre, insc_frais, membre_numero  NOT NULL
```

- **Dépendances fonctionnelles** : un membre peut effectuer plusieurs inscriptions (à des années différentes par exemple, ou pour changer de statut).

*insc_numero* → *insc_annee*

*insc_numero* → *status_membre*

*insc_numero* → *insc_frais*

*insc_numero* → *membre_numero*

*status_membre* → *insc_frais*

- **Clés candidates** : 

*insc_numero*

- **Clé primaire** : 

*insc_numero*

- **Preuve BCNF** : 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Ici, la clé numeroInscription n'est composée que d'un seul attribut, le problème ne se pose donc pas.

<u>3NF</u> : La relation n'est pas en 3NF puisque l’attribut *insc_frais* est déterminé par *status_membre* qui n’est pas une clé.

Transformation de 2NF en 3NF :

On sépare la table en deux tables *Inscription* et *FraisInscription* qui sont en BCNF :

```
Inscription (
    #insc_numero : integer, 
    insc_annee : integer(4), 
    status_membre=>FraisInscription,  
    membre_numero=>Membre
) avec insc_annee, membre_numero  NOT NULL

FraisInscription (
    #status_membre : {am, pro}, 
    insc_frais : {0,25}, 
) avec status_membre, insc_frais NOT NULL
```

 

## Membre

```
Membre (
    #membre_numero : integer, 
    nom : string, 
    prenom : string, 
    DdN : date, 
    adresse : string, 
    tel : integer(10), 
    etat_dossier : boolean
) avec nom, prenom, DdN, adresse, tel, etat_dossier NOT NULL
```

**Dépendances fonctionnelles** :

D'après notre note de clarification, deux membres ne peuvent pas avoir le même nom **et** prénom. De plus, deux membres peuvent habiter à la même adresse.

*membre_numero* → *nom*

*membre_numero* → *prenom*

*membre_numero* → *DdN*

*membre_numero* → *adresse*

*membre_numero* → *tel*

*membre_numero* → *etat_dossier*

*(nom,prenom)* → *membre_numero*

*(nom,prenom)* → *DdN*

*(nom,prenom)* → *adresse*

*(nom,prenom)* → *tel*

*(nom,prenom)* → *etat_dossier*

**Clés candidates** : 

*numeroMembre*

*(nom,prenom)*

**Clé primaire :** 

*numeroMembre*

**Preuve BCNF :** 

<u>1NF</u> : La relation possède une clé mais l'attribut *adresse* n'est pas atomique. C'est un attribut composé. On modifie donc la table : 

Transformation de 0NF en 1NF :

On modifie la table *Membre*

```
Membre (
    #membre_numero : integer, 
    nom : string, 
    prenom : string, 
    DdN : date, 
    numero_adresse : integer,
    rue : string,
    ville : string,
    tel : integer(10), 
    etat_dossier : boolean
) avec nom, prenom, DdN, adresse, tel, etat_dossier NOT NULL
```

<u>2NF</u> : Pas de problème, *nom* et *prenom* ne définissent pas d’autres attributs séparement. 

<u>3NF</u> : OK. Tous les attributs ne faisant pas partie d’une clé sont déterminés par un attribut clé.

<u>BCNF</u> : OK. Toutes les DFE sont issues d’une clé.



## Responsabilité

```
Responsabilite (
    #membre_numero=>Membre, 
    #activite_numero=>Activite, 
    resp_membre : {president, membre}
) avec resp_membre NOT NULL
```

**Dépendances fonctionnelles :**

*(membre_numero,activite_numero)* → *resp_membre*

**Clés candidates :** 

*(numeroMembre,numeroActivite)*

**Clé primaire :** 

*(numeroMembre,numeroActivite)*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Les attributs de la clé ne définissent pas d’autres attributs séparément. 

<u>3NF</u> : OK. Tous les attributs ne faisant pas partie d’une clé sont déterminés par un attribut clé.

<u>BCNF</u> : OK. Toutes les DFE sont issues d’une clé.



## Résultat

```
Resultat (
    #membre_numero=>Membre, 
    #epreuve_numero=>Epreuve, 
    #result_numero : integer, 
    result_date : date, 
    rang : integer, 
    points : integer
) avec result_date, rang, points NOT NULL
```

**Dépendances fonctionnelles** : on suppose que plusieurs sportifs peuvent occuper le même rang (en cas d'ex-aequo par exemple).

*result_numero* → *membre_numero*

*result_numero* → *epreuve_numero*

*result_numero* → *result_date*

*result_numero* → *rang*

*result_numero* → *points*

*(membre_numero,numeroEpreuve)* → *result_numero*

*(membre_numero,numeroEpreuve)* → *result_date*

*(membre_numero,numeroEpreuve)* → *rang*

*(membre_numero,numeroEpreuve)* → *points* 

**Clés candidates :** 

(*numeroResultat*)

*(membre_numero,epreuve_numero)*

**Clé primaire :** 

*(result_numero)*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK



## Spécialité

```
Specialite (
    #spe_numero : integer, 
    spe_nom : string, 
    discipline_numero=>Discipline
) avec spe_nom et discipline_numero NOT NULL
```

**Dépendances fonctionnelles :** deux spécialités peuvent porter le même nom (par exemple la spécialité "100m" dans les disciplines natation et course à pied).

*spe_numero* → *spe_nom*

*spe_numero* → *discipline_numero*

**Clés candidates :**

*spe_numero*

**Clé primaire :** 

*spe_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK 



## Activité

```
Activite (
	#activite_numero : integer, 
	activite_titre : string, 
	activite_type : {financement, competition}, 
	activite_adresse : string
) avec activite_titre, activite_type, activite_adresse NOT NULL
```

**Dépendances fonctionnelles :** on suppose que plusieurs activités peuvent porter le même type et avoir lieu à la même adresse.

*activite_numero* → *activite_titre*

*activite_numero* → *activite_adresse*

**Clés candidates :**

*activite_numero*

**Clé primaire :** 

*activite_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK



## Paiement

```
Paiement (
    #paiement_numero : integer, 
    paiement_montant : float, 
    insc_numero=>Inscription
) avec paiement_montant, insc_numero NOT NULL
```

**Dépendances fonctionnelles :**

*paiement_numero* → *paiement_montant*

*paiement_numero* → *insc_numero*

*insc_numero* → *paiement_montant*

*insc_numero* → *paiement_numero*

**Clés candidates :**

*paiement_numero*

*insc_numero*

**Clé primaire :** 

*paiement_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK



## Discipline 

```
Discipline (
    #discipline_numero : integer,
    discipline_nom : string
) avec discipline_nom NOT NULL
```

**Dépendances fontionnelles :**

*discipline_numero* → *discipline_nom*

*discipline_nom* → *discipline_numero*

**Clés candidates :**

*discipline_numero*

*discipline_nom*

**Clé primaire :** 

*discipline_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK 



## Financement

```
Financement (
    #activite_numero=>Activite, 
    date : date, 
    heure : time
) avec date, heure NOT NULL

```

**Dépendances fonctionnelles :** on suppose qu'il peut y avoir deux activités différentes à la même date et la même heure.

*activite_numero* → *date*

*activite_numero* → *heure*

**Clés candidates :**

*activite_numero*

**Clé primaire :** 

*activite_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK



## Compétition

```
Competition (
	#activite_numero=>Activite, 
	date_debut : date, 
	date_fin : date, 
	lieu : string
) avec date_debut, date_fin, lieu NOT NULL

```

**Dépendances fonctionnelles :** on suppose que plusieurs compétitions différentes peuvent avoir lieu au même endroit aux mêmes dates.

*activite_numero* → *date_debut* 

*activite_numero* → *date_fin* 

*activite_numero* → *lieu*

**Clés candidates :**

*activite_numero*

**Clé primaire :** 

*activite_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK



## Compétition Interne

```
CompetitionInterne (
    #activite_numero=>Activite,
    titre : varchar
)
```

**Dépendances fonctionnelles :** on suppose que deux compétitions internes peuvent porter le même titre.

*activite_numero* → *titre*

**Clés candidates :**

*activite_numero*

**Clé primaire :** 

*activite_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK 

## Compétition Externe

```
CompetitionExterne (
    #activite_numero=>Activite, 
    orga_numero=>OrganisateurExterne
) avec orga_numero NOT NULL
```

**Dépendances fonctionnelles :** 

*activite_numero* → *orga_numero*

**Clés candidates :**

*activite_numero*

**Clé primaire :**

*activite_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK 

## Epreuve

```
Epreuve (
    #epreuve_numero : integer, 
    #activite_numero=>Activite, 
    epreuve_date : date, 
    epreuve_heure : time, 
    spe_numero=>Specialite
) avec epreuve_date, epreuve_heure, spe_numero NOT NULL
```

**Dépendances fonctionnelles :**

*epreuve_numero* → *activite_numero*

*epreuve_numero* → *epreuve_date*

*epreuve_numero* → *epreuve_heure*

*epreuve_numero* → *spe_numero*

**Clés candidates :**

*epreuve_numero*

**Clé primaire :** 

*epreuve_numero*

**Preuve BCNF :** 

<u>1NF</u> : La démonstration est triviale, la relation possède une clé et tous les attributs de la table sont atomiques. 

<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK 

## Organisateur Externe

```
OrganisateurExterne (
    #orga_numero : integer, 
    orga_nom : string, 
    orga_adresse : string, 
    nom_responsable : string, 
    prenom_responsable : string, 
    tel_responsable : integer(10)
) avec orga_nom, orga_adresse, nom_responsable, prenom_responsable, tel_responsable NOT NULL

```

**Dépendances fonctionnelles :** on suppose qu'un responsable peut être responsable de plusieurs clubs. De plus, à une adresse peuvent résider plusieurs clubs (s'il s'agit d'un complexe sportif par exemple).

*orga_numero* → *orga_adresse*

*orga_numero* → *nom_responsable* 

*orga_numero* → *prenom_responsable*

*orga_numero* → *telephone_responsable*

**Clés candidates :**

*orga_numero*

**Clé primaire :** 

*orga_numero*

**Preuve BCNF :** 

<u>1NF</u> : La relation possède une clé. L'attribut *adresse* n'est pas atomique, il est multivalué.
Transformation de 0NF en 1NF :

On modifie la table *Organisateur Externe*

```
MOrganisateurExterne (
    #orga_numero : integer, 
    orga_nom : string, 
    numero_adresse : integer, 
    rue : string,
    ville : string,
    nom_responsable : string, 
    prenom_responsable : string, 
    tel_responsable : integer(10)
) avec orga_nom, orga_adresse, nom_responsable, prenom_responsable, tel_responsable NOT NULL
```


<u>2NF</u> : Pas de problème, les clés ne définissent pas d’autres attributs séparément.

<u>3NF</u> : OK

<u>BCNF</u> : OK 



## Conclusion

Toutes les relations de notre base de données étant maintenant en BCNF (donc en 3NF) on peut donc affirmer que la base de données est en 3NF.