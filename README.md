# NF17 Projet : TD du mardi après-midi, Groupe 4

**Sujet**
Un club sportif cherche à implémenter une base de données pour gérer automatiquement les dossiers de ses membres.

**Groupe de projet** Violette Quitral, Roxane Artinian, Xupeng Wang, Alice Berlose

**Livrables attendus**
*  NDC : *remis*
*  MCD : *remis*
*  NDR : *remis*
*  MLD : *remis*
*  SQL LLD : *remis*
*  SQL LMD : *remis*
*  POC Neo4J ou MongoDB
*  POC Oracle RO
*  POC Oracle/XML
*  POC PostgreSQL/JSON