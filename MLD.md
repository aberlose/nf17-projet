**Membre**(#membre_numero : integer, nom : string, prenom : string, DdN : date, adresse : string, tel : integer(10), etat_dossier : boolean) *avec nom, prenom, DdN, adresse, tel, etat_dossier NOT NULL*

**Activite**(#activite_numero : integer, activite_titre : string, activite_type : {financement, competition}, activite_adresse : string) *avec activite_titre, activite_type, activite_adresse NOT NULL*

**Responsabilite**(#membre_numero=>Membre, #activite_numero=>Activite, resp_membre : {president, membre}) *avec resp_membre NOT NULL*

**Epreuve**(#epreuve_numero : integer, #activite_numero=>Activite, epreuve_date : date, epreuve_heure : time, spe_numero=>Specialite) *avec epreuve_date, epreuve_heure, spe_numero NOT NULL*

**Resultat**(#membre_numero=>Membre, #epreuve_numero=>Epreuve, #result_numero : integer, result_date : date, rang : integer, points : integer) *avec result_date, rang, points NOT NULL*

**Inscription**(#insc_numero : integer, insc_annee : integer(4), status_membre : {am, pro}, insc_frais : {0,25}, membre_numero=>Membre) *avec insc_annee, status_membre, insc_frais, membre_numero  NOT NULL*

**Paiement**(#paiement_numero : integer, paiement_montant : float, insc_numero=>Inscription) *avec paiement_montant, insc_numero NOT NULL*

**Specialite**(#spe_numero : integer, spe_nom : string, discipline_numero=>Discipline) *avec spe_nom et discipline_numero NOT NULL*

**EstSpecialise**(#membre_numero=>Membre, #spe_numero=>Specialite)

**Discipline**(#discipline_numero : integer, discipline_nom : string) *avec discipline_nom NOT NULL*

**Financement**(#activite_numero=>Activite, date : date, heure : time) *avec date, heure NOT NULL*

**Competition**(#activite_numero=>Activite, date_debut : date, date_fin : date, lieu : string) *avec date_debut, date_fin, lieu NOT NULL*

**CompetitionInterne**(#activite_numero=>Activite)

**CompetitionExterne**(#activite_numero=>Activite, orga_numero=>OrganisateurExterne) *avec orga_numero NOT NULL*

**OrganisateurExterne**(#orga_numero : integer, orga_nom : string, orga_adresse : string, nom_responsable : string, prenom_responsable : string, tel_responsable : integer(10)} *avec orga_nom, orga_adresse, nom_responsable, prenom_responsable, tel_responsable NOT NULL*

Projection(Activite,numeroActivite) IN Union(Projection(Financement,numeroActivite),Projection(Competition,numeroActivite))
Intersection(Projection(Financement,numeroActivite),Projection(Competition,numeroActivite)) IS NULL