/* Scenario 1 : on souhaite afficher le nom et prénom de tous les membres
actifs du club.
On réalise un partitionnement vertical pour ne traiter que le numéro de membre,
le nom, le prénom et l'état de dossier. Puis un partitionnement horizontal pour
ne sélectionner que les dossiers actifs*/

/* Partitionnement vertical dans le fichier lld.sql : on divise la table membre en
2 tables distinctes */

CREATE TABLE MembreStatut (
numeroMembre INTEGER PRIMARY KEY,
nom VARCHAR,
prenom VARCHAR,
etatDossier NUMERIC(1)
);

CREATE TABLE Membre (
numeroMembre INTEGER PRIMARY KEY,
dateDeNaissance DATE NOT NULL,
numero_adresse INTEGER,
rue VARCHAR,
ville VARCHAR,
telephone NUMERIC(10),
FOREIGN KEY (numeroMembre) REFERENCES MembreStatut(numeroMembre)
);

/* Modification des INSERT dans le fichier insert.sql */

BEGIN TRANSACTION;
INSERT INTO MembreStatut (numeroMembre, nom, prenom, etatDossier)
VALUES (1,'Dupont','Bertrand',1);

INSERT INTO Membre (numeroMembre, dateDeNaissance, numero_adresse, rue, ville, telephone)
VALUES (1,TO_DATE('01/01/1951','DD/MM/YYYY'),33,'rue du Chene','Paris', 0612344567);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(1, 2017, 1,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(1,25,1);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO MembreStatut (numeroMembre, nom, prenom, etatDossier)
VALUES (2,'Pouillet','Salome',0);

INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (2,TO_DATE('29/09/1998','DD/MM/YYYY'),2,'rue du Rosier','Paris', 0696344567);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(2, 2017, 2,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(2,5,2);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO MembreStatut (numeroMembre, nom, prenom, etatDossier)
VALUES (3,'Auclour','Mathilde',1);

INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (3,TO_DATE('14/03/1997','DD/MM/YYYY'),6,'rue du Chevreuil','Paris', 0618544567);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(3, 2017, 3,'Amateur');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(3,25,3);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO MembreStatut (numeroMembre, nom, prenom, etatDossier)
VALUES (4,'Clarke','Spencer',1);

INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (4,TO_DATE('05/06/1989','DD/MM/YYYY'),12,'avenue de la Libération','Toulouse', 0664368902);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(4, 2018, 4,'Amateur');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(4,0,4);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO MembreStatut (numeroMembre, nom, prenom, etatDossier)
VALUES (5,'Huffman','Gabriella',1);

INSERT INTO Membre (numeroMembre, nom, prenom, dateDeNaissance, numero_adresse, rue, ville, telephone, etatDossier)
VALUES (5,TO_DATE('27/11/1976','DD/MM/YYYY'),68,'boulevard des États-Unis','Nantes', 0789451344);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(5, 2018, 5,'Amateur');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(5,0,5);
COMMIT TRANSACTION;

/* La requête correspondant au scenario 1 est donc la suivante : */
SELECT *
FROM MembreStatut
WHERE etatDossier=1;

/* Penser à ajouter un nouveau DROP dans drop.sql pour supprimer la nouvelle table */

DROP TABLE MembreStatut CASCADE;

/* ------------------------------------------------------------------------- */

/* Scenario 2 : on souhaite afficher tous les résultats du membre Bertrand Dupont
à partir de son nom */

CREATE VIEW vResultatsDupont AS
SELECT r.membre, r.rang, r.points
FROM Resultat r, Membre m
WHERE m.nom='Dupont' AND m.prenom='Bertrand' AND m.numeroMembre=r.membre;

SELECT * FROM vResultatsDupont;

/* ------------------------------------------------------------------------- */

/* Scenario 3 : on souhaite afficher le nom de tous les clubs organisateurs externes
se trouvant à Compiègne */

/* On réalise un partitionnement vertical pour ne traiter que le numéro d'organisateur,
le nom du club organisateur et la ville. Puis un partitionnement horizontal pour
ne sélectionner que les clubs organisateurs à Compiègne */

/* Partitionnement vertical dans le fichier lld.sql : on divise la table OrganisateurExterne en
2 tables distinctes */

CREATE TABLE villeOrganisateurExterne (
numeroOrganisateur INTEGER PRIMARY KEY,
nomOrganisateur VARCHAR NOT NULL,
villeOrganisateur VARCHAR
);

CREATE TABLE OrganisateurExterne (
numeroOrganisateur INTEGER PRIMARY KEY,
numAdresseOrganisateur INTEGER,
rueOrganisateur VARCHAR,
nomResponsable VARCHAR NOT NULL,
prenomResponsable VARCHAR NOT NULL,
telephoneResponsable NUMERIC(10) NOT NULL,
FOREIGN KEY (numeroOrganisateur) REFERENCES villeOrganisateurExterne(numeroOrganisateur)
);

/* Modification des INSERT dans le fichier insert.sql */

BEGIN TRANSACTION;
INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (2,'Activite du Mercredi','Mercieres');

INSERT INTO Competition (numeroActivite,dateDebut,dateFin, lieuCompetition)
VALUES(2,TO_DATE('30/03/2019','DD/MM/YYYY'), TO_DATE('03/04/2019','DD/MM/YYYY'), 'Lyon');

INSERT INTO villeOrganisateurExterne(numeroOrganisateur, nomOrganisateur, villeOrganisateur)
VALUES(1,'Club des Sportifs','Compiègne');

INSERT INTO OrganisateurExterne(numeroOrganisateur, nomOrganisateur,numAdresseOrganisateur,rueOrganisateur,villeOrganisateur,nomResponsable,prenomResponsable,telephoneResponsable)
VALUES(1,18,'rue du Chemin Vert', 'Benoit', 'Bastien', '0621887068');

INSERT INTO CompetitionExterne (numeroActivite,organisateurExterne)
VALUES(2, 1);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (4,'Championnats du monde de Natation','Moscou');

INSERT INTO Competition (numeroActivite,dateDebut,dateFin, lieuCompetition)
VALUES(4,TO_DATE('04/03/2018','DD/MM/YYYY'), TO_DATE('21/03/2018','DD/MM/YYYY'), 'Piscine Olympique de Moscou');

INSERT INTO villeOrganisateurExterne(numeroOrganisateur, nomOrganisateur, villeOrganisateur)
VALUES(2,'Club des nageurs russes','Moscou');

INSERT INTO OrganisateurExterne(numeroOrganisateur, nomOrganisateur,numAdresseOrganisateur,rueOrganisateur,villeOrganisateur,nomResponsable,prenomResponsable,telephoneResponsable)
VALUES(2,1465,'avenue du Kremlin', 'Sharapolov', 'Grigor', '0688349074');

INSERT INTO CompetitionExterne (numeroActivite,organisateurExterne)
VALUES(4,2);
COMMIT TRANSACTION;

/* La requête correspondant au scenario 3 est donc la suivante : */
SELECT *
FROM villeOrganisateurExterne
WHERE villeOrganisateur='Compiègne';

/* Penser à ajouter un nouveau DROP dans drop.sql pour supprimer la nouvelle table */

DROP TABLE villeOrganisateurExterne CASCADE;
