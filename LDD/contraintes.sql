-- Contrainte liée à l'héritage exclusif de la classe Activite
-- On s'assure que Intersection(Projection(Financement,numeroActivite),Projection(Competition,numeroActivite)) IS NULL.

CREATE VIEW vCheckExclusiviteActivite AS
SELECT * FROM vCompetition
INTERSECT
SELECT * FROM vFinancement;

-- Contrainte liée au fait que la classe Activite est abstraite
-- On vérifie que Projection(Activite,numeroActivite) IN Union(Projection(Financement,numeroActivite),Projection(Competition,numeroActivite)).

CREATE VIEW vCompetitionFinancementUnion AS
SELECT * FROM vCompetition
UNION
SELECT * FROM vFinancement;

CREATE VIEW vActiviteAbstraite AS -- On vérifiera une fois les tables crées que vActiviteAbstraite est bien VIDE
SELECT numeroActivite
FROM Activite
WHERE numeroActivite NOT IN (SELECT * FROM vCompetitionFinancementUnion);

-- Contrainte liée à l'héritage exclusif de la classe Competition
-- On vérifie que Intersection(Projection(CompetitionInterne,numeroActivite),Projection(CompetitionExterne,numeroActivite)) IS NULL

CREATE VIEW vCheckExclusiviteCompetition AS -- Vérification héritage exclusif Competition : vCheckExclusiviteCompetition doit être vide
SELECT * FROM vCompetitionInterne
INTERSECT
SELECT * FROM vCompetitionExterne;

-- Contrainte liée au fait que la classe Competition est abstraite
-- On vérifie que Projection(Competition,numeroActivite) IN Union(Projection(CompetitionInterne,numeroActivite),Projection(CompetitionExterne,numeroActivite))

CREATE VIEW vCompetitionAbstraiteUnion AS -- Competition classe abstraite (étape intermédiaire)
SELECT * FROM vCompetitionInterne
UNION
SELECT * FROM vcompetitionExterne;

CREATE VIEW vCompetitionAbstraite AS -- Competition classe abstraite : v doit être vide
SELECT numeroActivite
FROM Activite
WHERE numeroActivite NOT IN (SELECT * FROM vCompetitionAbstraiteUnion);

-- On vérifie que si l'on ajoute un résultat à un membre, alors celui-ci doit 
-- être actif, professionnel et inscrit à la spécialité de l'épreuve.
-- On réalise :
-- vActif = Restriction(Membre,etatDossier="a") // on vérifie que le membre est actif ;
-- vActifBis = Jointure(R1,Inscription,R1.numeroMembre = Inscription.membre) ;
-- vProfessionnel = Restriction(R2,R2.statutMembre = "professionnel") ;// on vérifie que le membre est un professionnel ;
-- vProfessionnelBis = Jointure(R3,Specialisation,R3.numeroMembre = Specialisation.membre) ;
-- vSpecialite = Jointure(R4,Epreuve,R5.specialite = Epreuve.specialite) ;
-- Puis on s'assure que Projection(Resultat,membre,epreuve) IN Projection(R5,numeroMembre,numeroEpreuve)

CREATE VIEW vActif AS
SELECT *
FROM Membre
WHERE etatDossier='a';

CREATE VIEW vActifBis AS
SELECT *
FROM vActif
INNER JOIN Inscription ON vActif.numeroMembre = Inscription.membre;

CREATE VIEW vProfessionnel AS
SELECT *
FROM vActifBis
WHERE statutMembre='professionnel';

ALTER TABLE vProfessionnel RENAME COLUMN membre TO numMembre; -- On renomme la colonne membre pour pouvoir faire la requête suivante

CREATE VIEW vProfessionnelBis AS
SELECT *
FROM vProfessionnel
INNER JOIN Specialisation ON vProfessionnel.numeroMembre = Specialisation.membre;

ALTER TABLE vProfessionnelBis RENAME COLUMN specialite TO spe; -- On renomme la colonne specialite pour pouvoir faire la requête suivante

CREATE VIEW vSpecialite AS
SELECT *
FROM vProfessionnelBis
INNER JOIN Epreuve ON vProfessionnelBis.spe=Epreuve.specialite;

CREATE VIEW vProjectionSpe AS
SELECT numeroMembre, numeroEpreuve
FROM vSpecialite;

CREATE VIEW vProjectionResultat AS
SELECT membre, numeroEpreuve
FROM Resultat;

CREATE VIEW vCheckActifProInscrit AS -- On vérifie une fois les tables crées que vCheckActifProInscrit est VIDE
SELECT membre
FROM vProjectionResultat
WHERE membre NOT IN (SELECT numeroMembre FROM vProjectionSpe);

-- On s'assure que si le membre s'inscrit en tant qu'amateur, alors ses frais 
-- d'inscription sont nuls et s'il s'inscrit en tant que professionnel, alors 
-- ses frais d'inscription sont de 25€. On réalise donc :
-- vStatAm = Restriction(Inscription,statutMembre="amateur") ;
-- vFraisAm = Restriction (Inscription,fraisInscription=0) ;
-- vStatPro = Restriction (Inscription,statutMembre="professionnel") ;
-- vFraisPro Restriction (Inscription,fraisInscription=25) ;
-- Puis on vérifie que Projection(R1,numeroInscription) = Projection(R2,numeroInscription) 
-- AND Projection(R3,numeroInscription) = Projection(R4,numeroInscription)

CREATE VIEW vStatAm AS
SELECT numeroInscription
FROM Inscription
WHERE statutMembre='amateur';

CREATE VIEW vFraisAm AS
SELECT numeroInscription
FROM Inscription
WHERE fraisInscription=0;

CREATE VIEW vStatPro AS
SELECT numeroInscription
FROM Inscription
WHERE statutMembre='professionnel';

CREATE VIEW vFraisPro AS
SELECT numeroInscription
FROM Inscription
WHERE fraisInscription=25;

CREATE VIEW vCheckFraisAmateur AS -- On vérifie une fois les tables créées que vCheckFraisAmateur est VIDE
SELECT numeroInscription
FROM vStatAm
WHERE numeroInscription NOT IN (SELECT * FROM vFraisAm);

CREATE VIEW vCheckFraisPro AS -- On vérifie une fois les tables créées que vCheckFraisAmateur est VIDE
SELECT numeroInscription
FROM vStatPro
WHERE numeroInscription NOT IN (SELECT * FROM vFraisPro);
