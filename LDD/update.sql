/* Gestion des UPDATE par des transactions */
/* Lorsqu'un membre professionnel finit de payer son inscription (montantPaiement=25),
l'état de son dossier passe au statut Actif (=1)*/

BEGIN TRANSACTION;
UPDATE Paiement
SET montantPaiement=25
WHERE inscription=2;

CREATE VIEW vStatutMembre AS
SELECT i.numeroInscription
FROM Inscription i, Membre m
WHERE i.membre=m.numeroMembre AND i.numeroInscription=2;

UPDATE Membre
SET etatDossier=1
WHERE (SELECT * FROM vStatutMembre)=2;
COMMIT TRANSACTION;

/* Lors d'une compétition, on peut mettre à jour les résultats (points et rang)
d'un sportif.
Exemple ici avec la mise à jour des résultats du membre n°1 pour l'épreuve n°1.*/

BEGIN TRANSACTION;
UPDATE Resultat
SET points=257
WHERE membre=1 AND numeroEpreuve=1;

UPDATE Resultat
SET rang=4
WHERE membre=1 AND numeroEpreuve=1;
COMMIT TRANSACTION;