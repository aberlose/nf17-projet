--Requêtes

--Chercher le nom et prénom de tous les membres dont le dossier est actif (numéro de dossier = 1)

SELECT infos_noms->>'nom' AS nom_membre,
infos_noms->>'prenom' AS prenom_membre
FROM Membre
WHERE etatdossier = 1;


--Chercher le nom des équipes qui participent à une compétition qui a lieu à Päris

SELECT equipes->>'nom_equipe' AS equipe
FROM Competition
WHERE lieu = 'Paris';




SELECT infos->'telephones'->>'domicile'
FROM Membre
WHERE infos->'adresses'->>'ville' = 'Paris';


-- Chercher nom/prénom des membres spécialisés dans la brasse

SELECT infos_noms->>'nom', infos_noms->>'prenom'
FROM Membre, EstSpecialise, Specialite
WHERE Specialite.spe_nom = 'Brasse'
AND Membre.membre_numero = EstSpecialise.membre_numero AND  Specialite.spe_numero = EstSpecialise.spe_numero
GROUP BY  infos_noms->>'nom', infos_noms->>'prenom';

