DECLARE
refMembre REF tMembre;
refInscription REF tInscription;
refActivite REF tActivite;
refMembre2 REF tMembre;
refMembre3 REF tMembre;
refMembre4 REF tMembre;
refMembre5 REF tMembre;
refMembre6 REF tMembre;
refDiscipline1 REF tDiscipline;
refDiscipline2 REF tDiscipline;
refSpecialite4 REF tSpecialite;
refEpreuve REF tEpreuve;

BEGIN

INSERT INTO Membre (membre_numero, infos_nom,infos, etat_dossier)
VALUES (1, '{"nom":"Dupont", "prenom" : "Bertrand"}', '{"adresses" : {"numero":18,"rue":"rue St Fiacre","ville":"Compiègne"}, "telephones" : {"bureau" : 0171285513, "domicile" : 0612344567}}', 1);


SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(1, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 1;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(1,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, infos_nom,infos, etat_dossier)
VALUES (7,'{"nom":"Salut", "prenom" : "Jeanne"}', '{"adresses" : {"numero":15,"rue":"rue St Jolie","ville":"Paris"}, "telephones" : {"bureau" : 0171266513, "domicile" : 0612377567}}', 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 7;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(7, 2019, 'pro', 25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 7;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(7,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, infos_nom,infos, etat_dossier)
VALUES (3,'{"nom":"Poulette", "prenom" : "Mireille"}', '{"adresses" : {"numero":1,"rue":"rue du Port","ville":"Amiens"}, "telephones" : {"bureau" : 0178985513, "domicile" : 0612341067}}', 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 3;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(3, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 3;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(3,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, infos_nom,infos, etat_dossier)
VALUES (4,'{"nom":"Quitral", "prenom" : "Violette"}', '{"adresses" : {"numero":2,"rue":"rue du Nord","ville":"Paris"}, "telephones" : {"bureau" : 0171854513, "domicile" : 0612343697}}', 1);


SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 4;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(4, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 4;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(4,25,refInscription);

END;
/
BEGIN

INSERT INTO Membre (mmembre_numero, infos_nom,infos, etat_dossier)
VALUES (5,'{"nom":"Artitian", "prenom" : "Roxy"}', '{"adresses" : {"numero":47,"rue":"rue du Sud","ville":"Paris"}, "telephones" : {"bureau" : 0171800513, "domicile" : 0612000697}}', 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 5;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(5, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 5;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(5,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, infos_nom,infos, etat_dossier)
VALUES (6,'{"nom":"Prout", "prenom" : "Vio"}', '{"adresses" : {"numero":89,"rue":"rue du JAloux","ville":"Paris"}, "telephones" : {"bureau" : 0178854513, "domicile" : 0612223697}}', 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Inscription (insc_numero, insc_annee, status_membre, insc_frais, membre_numero)
VALUES(6, 2019, 'pro', 25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 6;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(6, 25, refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, infos_nom,infos, etat_dossier)
VALUES (2,'{"nom":"Tramp", "prenom" : "Pauline"}', '{"adresses" : {"numero":78,"rue":"rue du Fun","ville":"Paris"}, "telephones" : {"bureau" : 0177774513, "domicile" : 061277797}}', 1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 2;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(2, 2019, 'am' ,0, refMembre);
END;
/

BEGIN

INSERT INTO Activite (activite_numero,activite_titre, activite_type, activite_adresse)
VALUES (1,'Activite du Lundi','competition',5,'Rue de la Rue','Compiègne');

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Competition (activite_numero,date_debut,date_fin,lieu,equipes)
VALUES(refActivite,TO_DATE('18/04/2019','DD/MM/YYYY'), TO_DATE('20/04/2019','DD/MM/YYYY'), 'Paris',
'{"equipe" : { "nom_equipe" : "Les Grands", "membres" : { "membre" : refMembre, "membre" : refMembre1, "membre" : refMembre2}}, "equipe" : { "nom_equipe" : "Les Petits", "membres" : { "membre" : refMembre3, "membre" : refMembre4, "membre" : refMembre5}}}');

INSERT INTO CompetitionInterne (activite_numero)
VALUES(1);

END;
/

BEGIN

INSERT INTO Discipline(discipline_numero,discipline_nom)
VALUES(1, 'Natation');

INSERT INTO Discipline(discipline_numero,discipline_nom)
VALUES(2, 'Cyclisme');

SELECT REF(d) INTO refDiscipline1
FROM Discipline d
WHERE discipline_numero = 1;

SELECT REF(d) INTO refDiscipline2
FROM Discipline d
WHERE discipline_numero = 2;

INSERT INTO Specialite(spe_numero, spe_nom, discipline_numero)
VALUES(1, 'Criterium', refDiscipline1);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(2, 'Brasse', refDiscipline2);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(3, '100 m', refDiscipline1);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(4, 'Contre la montre', refDiscipline2);

END;
/

INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(1,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(3,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(4,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(5,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(6,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(7,4);

BEGIN

SELECT REF(s) INTO refSpecialite4
FROM Specialite s
WHERE spe_numero = 4;

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

INSERT INTO Epreuve (epreuve_numero, activite_numero, horaire, spe_numero)
VALUES(1,refActivite,TO_DATE('18/04/2019 15:00:00','DD/MM/YYYY HH24:MI:SS'), refSpecialite4);

END;
/

BEGIN

SELECT REF(e) INTO refEpreuve
FROM Epreuve e
WHERE epreuve_numero = 1;

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre,refEpreuve,1,6,10);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre2,refEpreuve,2,4,40);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre3,refEpreuve,3,2,80);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre4,refEpreuve,4,1,100);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre5,refEpreuve,4,5,20);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre6,refEpreuve,5,3,60);

END;
/

BEGIN

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre, refActivite, 'president');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre2, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre3, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre4, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre5, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre6, refActivite, 'membre');

END;

/
