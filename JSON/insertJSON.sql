INSERT INTO Membre (numeroMembre,infos_noms,infos,etatDossier)
VALUES (1,'{"nom":"Poulette", "prenom" : "Mireille"}', '{"adresses" : {"numero":1,"rue":"rue du Port","ville":"Amiens"}, "telephones" : {"bureau" : 0178985513, "domicile" : 0612341067}}', 1);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(1, 2017, 1,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(1,25,1);

INSERT INTO Membre (numeroMembre,infos_nom,infos,etatDossier)
VALUES (2,'{"nom":"Poulette", "prenom" : "Mireille"}', '{"adresses" : {"numero":1,"rue":"rue du Port","ville":"Amiens"}, "telephones" : {"bureau" : 0178985513, "domicile" : 0612341067}}', 1);

INSERT INTO Inscription (numeroInscription,anneeInscription,membre, statutMembre)
VALUES(2, 2017, 2,'Professionnel');

INSERT INTO Paiement (numeroPaiement, montantPaiement,inscription)
VALUES(2,5,2);

INSERT INTO Activite (numeroActivite,titreActivite, adresseActivite)
VALUES (1,'Activite du Lundi','Clos des Roses');

INSERT INTO Competition (numeroActivite,
dateDebut,
dateFin,
lieuCompetition,
equipes)
VALUES(1,TO_DATE('18/04/2019','DD/MM/YYYY'), TO_DATE('20/04/2019','DD/MM/YYYY'), 'Paris',
'{"equipe" : { "nom_equipe" : "Les Grands", "membres" : { "membre" : 1, "membre" : 2, "membre" : 3}}, "equipe" : { "nom_equipe" : "Les Petits", "membres" : { "membre" : 4, "membre" : 5, "membre" : 6}}}');


INSERT INTO Discipline(numeroDiscipline,nomDiscipline)
VALUES(3, 'Natation');

INSERT INTO Specialite(numeroSpecialite,nomSpecialite,discipline)
VALUES(4, 'Brasse', 3);

INSERT INTO Epreuve (competition,numeroEpreuve,dateEpreuve,heureEpreuve,specialite)
VALUES(1,1,TO_DATE('18/04/2019','DD/MM/YYYY'),'15:00:00', 4);
