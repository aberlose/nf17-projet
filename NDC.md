# Note de clarification

Un club sportif souhaite implémenter une base de données de façon à gérer plus facilement ses membres et suivre les activités auxquelles ils participent. Pour que le projet soit intéressant à réaliser, on supposera qu’il s’agit d’un club “multisports” proposant un large choix de sports et d’activités à ses membres.

Les personnes qui seront amenées à manipuler cette base de données sont soit la direction du club, un entraîneur ou le personnel administratif du club.

Ce document constitue une reformulation du cahier des charges, à laquelle ont été apportées quelques précisions. Les informations sont ordonnées de telle sorte à ce que l’implémentation de la base de données soit la plus logique possible.

Cette note de clarification sera revue à chaque modification majeure apportée au projet.

## Premier aspect : la gestion des membres

1. ### L’inscription d’un membre

L’inscription d’un membre est caractérisée par l’année d’inscription ainsi que le statut professionnel qu’occupe le membre à son inscription. Ce dernier attribut peut prendre deux valeurs : amateur ou professionnel.

On suppose que les inscriptions au club sont à l’année : un membre ne peut donc s’inscrire qu’une fois par an et une inscription ne relève que d’un membre en particulier. 

Nous distinguons deux cas de figure : soit c’est un nouveau membre et dans ce cas, le membre administratif doit procéder à la saisie d’informations personnelles. Sinon, le membre administratif peut directement rechercher dans son dossier en se basant soit sur des coordonnées personnelles (nom/prénom), soit avec le numéro de membre.

2. ### Les membres

Après sa première inscription au club, un membre existe de façon unique dans la base de données grâce à un numéro de membre généré automatiquement par le système informatique. En plus de cet identifiant unique, chaque membre possède des informations personnelles saisies lors de son inscription à savoir un nom, un prénom, une date de naissance, une adresse, et un numéro de téléphone. Enfin, un membre possède un statut administratif qui est soit actif, soit inactif (en fonction du règlement ou non de ses frais d’inscription).

On suppose que lorsqu’un membre se réinscrit, il conserve le même dossier (qui est mis-à-jour) et donc le même numéro unique par lequel il peut être retrouvé dans la base. Les membres du club qui sont gérés par la base de données sont uniquement les sportifs (la base ne sert pas à gérer les entraîneurs, l’administration…).

#### a) Membre professionnel

Un membre professionnel doit s’acquitter de frais d’inscription d’un montant de 25€. L’inscription d’un membre n’est finalisée qu’une fois le paiement effectué, donc lorsque le solde du membre est égal à 0. C’est à cet instant que le statut administratif du membre passe à la valeur active. Le solde est calculé en soustrayant le montant payé par le membre aux frais d’inscription.

#### b) Membre amateur

Un membre amateur ne doit payer aucun frais d’inscription (inscription à 0€) : son statut administratif est donc immédiatement actif.

3. ### Les disciplines

Les membres du club exercent une ou plusieurs disciplines sportives. Une discipline sportive possède un titre unique et englobe des spécialités. 

4. ### Les spécialités

Une discipline comprend une ou plusieurs spécialités : un sportif pratique au moins une spécialité et peut s’entraîner à plusieurs spécialités au sein de sa/ses discipline(s). Par exemple, un sportif qui exercerait la discipline course à pieds peut choisir comme spécialités le 100m ou le 5000m.

Les spécialités sont caractérisées par leur titre unique.



## Deuxième aspect : la gestion des activités

1. ### Les activités

Il existe deux types d’activités : des activités de financement ou des compétitions. Chaque activité existe de façon unique dans la base de données grâce à un numéro généré par le système. Une activité possède donc un numéro, un titre et a lieu à une adresse précise.

Concernant la création ou la modification d’une activité, c’est le directeur du comité d’organisation qui a accès à toutes les informations (titre de l’activité et données sur le directeur, ainsi que éventuellement les membres du membres du comité avec leur responsabilité).

#### 		a) Les activités de financement

Tout membre du club peut participer à l’organisation des activités de financement. Une activité de type financement est décrite par son numéro d’activité, par la date, l’heure et l'adresse où a lieu l'activité.

#### 		b) Les activités de compétition

Comme toute activité, une compétition possède un numéro unique généré par le système et a lieu dans un endroit précis. Une compétition sportive peut durer plusieurs jours (le temps d’un week-end par exemple). Elle a donc une date de début, une durée (en nombre de jours) et une date de fin. La date de fin est simplement calculée en ajoutant la durée à la date de début de la compétition.

Seuls les membres professionnels sont autorisés à s’inscrire en compétition.

##### Compétitions internes

Deux types de compétitions existent. Les compétitions internes ont lieu au sein du club. Les participants aux compétitions internes sont des professionnels actifs qui s'entraînent dans la spécialité sur laquelle porte la compétition.

##### Compétitions externes

Pour les compétitions à l’extérieur du club, un comité d’organisation local sélectionne les participants parmi une liste de sportifs déjà inscrits dans d’autres épreuves de la compétition. S’il n’est encore inscrit à aucune épreuve, alors le nom et le prénom du participant à la compétition seront rentrés manuellement dans la base de données. Un nouveau numéro unique est généré automatiquement à chaque nouveau participant à une compétition externe.

Dans le cas d’une compétition externe, la base de données contient également des informations sur le club organisateur : nom du club, responsable du club, coordonnées.

2. ### Les épreuves

Plusieurs épreuves peuvent avoir lieu lors d’une compétition. Une épreuve est caractérisée par sa date, son heure, son lieu, sa discipline et sa spécialité. Pour qu’une épreuve ait lieu, un participant au moins doit y participer.

3. ### Les résultats

Toute compétition induit des résultats. On a donc des résultats qui proviennent des compétition externes et d’autres des compétitions internes. Un résultat est associé à un sportif en particulier pour une épreuve particulière. Le résultat correspond à la position du sportif dans le classement final de la compétition.

4. ### Le comité d’organisation

Toute activité (de financement ou de compétition) est organisée par un comité d’organisation constitué de plusieurs des membres actifs du club, qu’ils soient professionnels ou amateurs. Un comité est composé d’un président et de simples membres.



## Fonctionnalités de la base de données

- La base doit permettre à un membre administratif d’inscrire un nouveau membre. Si ce membre a déjà fait partie du club, on doit pouvoir le retrouver grâce à son nom et prénom ou grâce à son numéro de membre et ainsi modifier son ancien dossier. On suppose donc qu’il n’y a pas deux membres du club qui possèdent le même nom et prénom. Si le membre n’a jamais été inscrit au club, on doit pouvoir saisir ses coordonnées et le système doit lui générer automatiquement un numéro unique.

- La base doit permettre d’afficher le dossier d’un membre contenant ses coordonnées ainsi que les activités auxquelles il a participé et ses résultats en compétition.

- On doit pouvoir ajouter, supprimer ou modifier les spécialités d’un sportif.

- On doit pouvoir saisir une nouvelle activité ou en modifier ses données. Le système doit générer un numéro unique pour chaque activité. La base doit aussi permettre de savoir quels membres ont participé à quelle activité et à quel titre (en tant que simple participant ou en tant que comité organisateur).

- Le système doit permettre d’ajouter une nouvelle épreuve et y ajouter des participants.