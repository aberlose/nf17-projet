# Note de révision

### On souhaite avoir pour cible non plus du relationnel mais du orienté-document. 

On modifie notre MCD pour avoir un UML qui soit adaptée à du orienté-document. Pour cela, il va falloir changer certaines des suppositions que l’on avait faites dans notre note de clarification destinée à l’implémentation d’une base de données relationnelle.

On modifie les associations entre les classes Membre, Inscription et Paiement. Un membre peut effectuer plusieurs inscriptions (une par an) et une inscription précise n’existerait pas sans le membre en question : on peut donc avoir une composition entre Membre et Inscription.
En non-relationnel, les attributs n'ont plus besoin d'être atomique : il est donc plus utile de retrouver l'adresse dans un unique attribut de type string.

De plus, chaque inscription peut être payée en plusieurs fois et un paiement n’a pas lieu d’être sans inscription. Donc on peut aussi avoir une composition entre Inscription et Paiement.

On peut également maintenant supposer qu’une spécialité n’appartient qu’à une unique discipline. Ainsi, la spécialité disparaît si la discipline est supprimée : il y a donc une relation de composition entre Discipline et Spécialité.

Enfin, on peut modifier les associations et relations autour de Activité et Compétition. En effet, on suppose que les classes CompétitionInterne et CompétitionExterne n'existent plus : on précise simplement le type de la Compétition grâce à un attribut. Pour une compétition, on peut donc avoir un OrganisateurExterne et différentes épreuves.

La nouvelle implémentation de notre BDD en non-relationnel nous permettra lors de requêtes d'obtenir un résultat sans avoir à faire de multiples jointures.