db.Membre.drop()

db.Membre.insert({
    _id:MembreId("590c366d70f50381c920ca71"),
    nom : "Dupont",
    prenom : "Bertrand",
    dateDeNaissance : "01/01/1951",
    adresse : "33 rue du Chene",
    ville : "Paris",
    telephone : "0612344567",
    etatDossier : 1,
    inscription : [
        {   idIscription : 1,
            anneeInscription : 2017,
            statutMembre : "Professionnel",
            frais : 25,
            paiement : [
                {
                    idPaiement : 1,
                    montantPaiement : 25
                }
            ]
        }
    ],
    specialiseDans : [
        {   idSpecialite : 1,
            nomSpecialite : "100m nage libre",
            idDiscipline : 1,
            nomDiscipline : "Natation"
        }
    ],
    participeA : [
        {
            idActivite : 1,
            titre : "Championnats du monde de natation",
            type : "competition",
            typeCompetition : "externe",
            adresse : "Moscou",
            lieu : "Piscine municipale",
            dateDebut : "18/04/2017",
            dateFin : "20/04/2017",
            organisateurExterne :
                {
                    idOrganisateur : "1",
                    nom : "Club des nageurs russes",
                    adresse : "1465 avenue du Kremlin",
                    ville : "Moscou",
                    nom_responsable : "Sharapolov",
                    prenom_responsable : "Grigor",
                    tel_responsable : "0688349074"
                },
            epreuve : [
                {
                    idEpreuve : 1,
                    date : "19/04/2019",
                    heure : "15:00:00",
                    resultat : [
                        {
                            idResultat : 1,
                            rang : 2,
                            points : 477
                        }
                    ]
                }
            ]
        }
    ],
    organise : [
        {
            idActivite : 3,
            titre : "Financement des championnats d'Europe",
            type : "financement",
            adresse : "Halle des sports",
            date : "15/09/2018",
            heure : "15:15:00",
            responsabilite : "Membre"
        }
    ]
})



db.Membre.insert(
  {
    _id:MembreId("590c366d70f50381c920ca71"),
    nom : "Watson",
    prenom : "John",
    dateDeNaissance : "13/11/1974",
    adresse : "21 avenue de la Libération",
    ville : "Toulouse",
    telephone : "0676246900",
    etatDossier : 1,
    inscription : [
        { 
            idIscription : 2,
            anneeInscription : 2018,
            statutMembre : "Amateur",
            frais : 0,
            paiement : [
                {
                    idPaiement : 2,
                    montantPaiement : 0
                }
            ]
        },
        { 
            idInscription : 3,
            anneeInscription : 2019,
            statutMembre : "Professionnel",
            frais : 25,
            paiement : [
                {
                    idPaiement : 3,
                    montantPaiement : 10
                },
                {
                    idPaiement : 4,
                    montantPaiement : 15
                }
            ]
        }
    ],
    specialiseDans : [
        { 
            idSpecialite : 2,
            nomSpecialite : "1000m",
            idDiscipline : "2",
            nomDiscipline : "Course à pied"
        }
    ],
    participeA : [
        {
            idActivite : 2,
            titre : "Championnats d'Europe d'athlétisme",
            type : "competition",
            typeCompetition : "externe",
            adresse : "Londres",
            dateDebut : "04/01/2019",
            dateFin : "15/01/2019",
            lieu : "Piste des sablons",
            organisateurExterne : 
                {
                    idOrganisateur : "1",
                    nom : "Club des athlètes anglais",
                    adresse : "221 Baker Street",
                    ville : "Londres",
                    nom_responsable : "Lestrades",
                    prenom_responsable : "Greg",
                    tel_responsable : "0634652389"
                },
            epreuve : [
                {
                    idEpreuve : 2,
                    date : "10/01/2019",
                    heure : "14:30:00",
                    resultat : [
                        {
                            idResultat : 2,
                            rang : 4,
                            points : 798
                        }
                    ]
                }
            ]
        }
    ]
})
