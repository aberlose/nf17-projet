# Note de révision

Cette note de révision a pour but de clarifier certains points concernant la première note de clarification. La réalisation du modèle conceptuel de données nous a permis de préciser les points suivants :

- **Paragraphe membres** : distinction nette entre membre amateur et membre professionnel : un membre professionnel a des frais d'inscription que le membre amateur n'a pas. Il a donc également un solde qui évolue en fonction du montant déjà payé.

- **Paragraphe compétition** : précision de la durée d’une compétition sportive : une compétition sportive peut finalement se dérouler sur plusieurs jours. Nous avons donc ajouté une date de début de la compétition, une durée (en nombre de jours) et une date de fin calculée à partir de la durée et de la date de début.

- **Paragraphe compétition** : distinction nette entre compétition interne et externe. Lors d'une compétition externe, un nouvel identifiant unique est généré automatiquement par le système pour le sportif.

- **Paragraphe résultats** : l'implémentation de cette base de données doit permettre d'accéder aux résultats d'un sportif pour une spécialité en particulier, qu'il s'agisse d'une compétition interne ou externe.

- **Paragraphes discipline et spécialité** : précision du caractère unique du titre d'une discipline et d'une spécialité.

Quelques paragraphes ont également été reformulés et restructurés pour faciliter la compréhension.