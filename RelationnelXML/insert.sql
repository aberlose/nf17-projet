DECLARE 
refMembre REF tMembre;
refInscription REF tInscription;
refActivite REF tActivite;
refMembre2 REF tMembre;
refMembre3 REF tMembre;
refMembre4 REF tMembre;
refMembre5 REF tMembre;
refMembre6 REF tMembre;
refDiscipline1 REF tDiscipline;
refDiscipline2 REF tDiscipline;
refSpecialite4 REF tSpecialite;
refEpreuve REF tEpreuve;

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (1, XMLTYPE('
        <infos_nom>
            <nom>Dupont</nom>
            <prenom>Bertrand</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>33</numero>
		        <rue>rue du Chêne</rue>
		        <ville>Paris</ville>
        	</adresses>
        	<adresses>
		        <numero>34</numero>
		        <rue>rue de la feuille</rue>
		        <ville>Meulun</ville>
        	</adresses>
	        <telephones>
		        <bureau>0171285513</bureau>
		        <domicile>0612344567</domicile>
        	</telephones>
	        <age>53</age>
        </infos>'), 
    1);


SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(1, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 1;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(1,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (7,XMLTYPE('
        <infos_nom>
            <nom>Winehouse</nom>
            <prenom>Amie</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>22</numero>
		        <rue>rue du Gros Sapin</rue>
		        <ville>Paris</ville>
        	</adresses>
        	<adresses>
		        <numero>25</numero>
		        <rue>rue du Petit Sapin</rue>
		        <ville>Palis</ville>
        	</adresses>
	        <telephones>
		        <bureau>0144283745</bureau>
		        <domicile>0692314573</domicile>
        	</telephones>
	        <age>25</age>
        </infos>'), 
    1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 7;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(7, 2019, 'pro', 25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 7;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(7,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (3,XMLTYPE('
        <infos_nom>
            <nom>Sapristi</nom>
            <prenom>Poulet</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>2</numero>
		        <rue>rue du Paradis</rue>
		        <ville>Paris</ville>
        	</adresses>
        	<adresses>
		        <numero>27</numero>
		        <rue>rue du Gros Lapin</rue>
		        <ville>Ris Orangis</ville>
        	</adresses>
	        <telephones>
		        <bureau>0172345513</bureau>
		        <domicile>0612383467</domicile>
        	</telephones>
	        <age>12</age>
        </infos>'), 
    1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 3;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(3, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 3;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(3,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (4,XMLTYPE('
        <infos_nom>
            <nom>Tropbelle</nom>
            <prenom>Jeanne</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>5</numero>
		        <rue>rue du Chat</rue>
		        <ville>Vienne</ville>
        	</adresses>
        	<adresses>
		        <numero>8</numero>
		        <rue>rue du Chien</rue>
		        <ville>Graz</ville>
        	</adresses>
	        <telephones>
		        <bureau>0164738474</bureau>
		        <domicile>0673827363</domicile>
        	</telephones>
	        <age>19</age>
        </infos>'), 
    1);


SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 4;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(4, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 4;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(4,25,refInscription);

END;
/
BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (5,XMLTYPE('
        <infos_nom>
            <nom>Blonde</nom>
            <prenom>Jeanne</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>45</numero>
		        <rue>rue du Poteau</rue>
		        <ville>Paris</ville>
        	</adresses>
        	<adresses>
		        <numero>78</numero>
		        <rue>rue du Loup</rue>
		        <ville>Compi</ville>
        	</adresses>
	        <telephones>
		        <bureau>0125364756</bureau>
		        <domicile>0657392800</domicile>
        	</telephones>
	        <age>19</age>
        </infos>'), 
    1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 5;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(5, 2019, 'pro',25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 5;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(5,25,refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (6,XMLTYPE('
        <infos_nom>
            <nom>Mauve</nom>
            <prenom>Berthe</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>18</numero>
		        <rue>rue de la Chance</rue>
		        <ville>Buzançais</ville>
        	</adresses>
        	<adresses>
		        <numero>45</numero>
		        <rue>rue de la rédemption</rue>
		        <ville>Besancon</ville>
        	</adresses>
	        <telephones>
		        <bureau>0154368999</bureau>
		        <domicile>0111821822</domicile>
        	</telephones>
	        <age>84</age>
        </infos>'), 
    1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Inscription (insc_numero, insc_annee, status_membre, insc_frais, membre_numero)
VALUES(6, 2019, 'pro', 25, refMembre);

SELECT REF(i) INTO refInscription
FROM Inscription i
WHERE insc_numero = 6;

INSERT INTO Paiement (paiement_numero, paiement_montant,insc_numero)
VALUES(6, 25, refInscription);

END;
/

BEGIN

INSERT INTO Membre (membre_numero, nom, prenom, ddN, adresse, tel, etat_dossier)
VALUES (2,XMLTYPE('
        <infos_nom>
            <nom>Aubry</nom>
            <prenom>Salomé</prenom>
        </infos_nom>'), 
    XMLTYPE('
        <infos>
	        <adresses>
		        <numero>90</numero>
		        <rue>Rue de Pierrick</rue>
		        <ville>Lorient</ville>
        	</adresses>
        	<adresses>
		        <numero>75</numero>
		        <rue>Rue de la rue</rue>
		        <ville>Brest</ville>
        	</adresses>
	        <telephones>
		        <bureau>092847564</bureau>
		        <domicile>0654289574</domicile>
        	</telephones>
	        <age>20</age>
        </infos>'), 
    1);

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 2;

INSERT INTO Inscription (insc_numero,insc_annee, status_membre, insc_frais, membre_numero)
VALUES(2, 2019, 'am' ,0, refMembre);
END;
/

BEGIN 

INSERT INTO Activite (activite_numero,activite_titre, activite_type, activite_adresse)
VALUES (1,'Activite du Lundi','competition',5,'Rue de la Rue','Compiègne');

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Competition (activite_numero,date_debut,date_fin,lieu,equipes)
VALUES(refActivite,TO_DATE('18/04/2019','DD/MM/YYYY'), TO_DATE('20/04/2019','DD/MM/YYYY'), 'Paris', 
XMLTYPE('<equipes>
	<equipe>
		<nom_equipe>Les Grands</nom_equipe>
		<membres>
		    <membre> refMembre </membre>
		    <membre> refMembre1 </membre>
		    <membre> refMembre2 </membre>
		</membres>
	</equipe>
	<equipe>
		<nom_equipe>Les Petits</nom_equipe>
		<membres>
		    <membre> refMembre3 </membre>
		    <membre> refMembre4 </membre>
		    <membre> refMembre5 </membre>
		</membres>
	</equipe>
</equipes>'));

INSERT INTO CompetitionInterne (activite_numero)
VALUES(1);

END;
/

BEGIN

INSERT INTO Discipline(discipline_numero,discipline_nom)
VALUES(1, 'Natation');

INSERT INTO Discipline(discipline_numero,discipline_nom)
VALUES(2, 'Cyclisme');

SELECT REF(d) INTO refDiscipline1
FROM Discipline d
WHERE discipline_numero = 1;

SELECT REF(d) INTO refDiscipline2
FROM Discipline d
WHERE discipline_numero = 2;

INSERT INTO Specialite(spe_numero, spe_nom, discipline_numero)
VALUES(1, 'Criterium', refDiscipline1);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(2, 'Brasse', refDiscipline2);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(3, '100 m', refDiscipline1);

INSERT INTO Specialite(spe_numero,spe_nom,discipline_numero)
VALUES(4, 'Contre la montre', refDiscipline2);

END;
/

INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(1,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(3,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(4,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(5,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(6,4);
INSERT INTO EstSpecialise(membre_numero, spe_numero)
VALUES(7,4);

BEGIN

SELECT REF(s) INTO refSpecialite4
FROM Specialite s 
WHERE spe_numero = 4;

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

INSERT INTO Epreuve (epreuve_numero, activite_numero, horaire, spe_numero)
VALUES(1,refActivite,TO_DATE('18/04/2019 15:00:00','DD/MM/YYYY HH24:MI:SS'), refSpecialite4);

END;
/

BEGIN

SELECT REF(e) INTO refEpreuve
FROM Epreuve e 
WHERE epreuve_numero = 1;

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre,refEpreuve,1,6,10);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre2,refEpreuve,2,4,40);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre3,refEpreuve,3,2,80);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre4,refEpreuve,4,1,100);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre5,refEpreuve,4,5,20);

INSERT INTO Resultat (membre_numero,epreuve_numero,result_numero,rang,points)
VALUES(refMembre6,refEpreuve,5,3,60);

END;
/

BEGIN

SELECT REF(m) INTO refMembre
FROM Membre m
WHERE membre_numero = 1;

SELECT REF(m) INTO refMembre2
FROM Membre m
WHERE membre_numero = 7;

SELECT REF(m) INTO refMembre3
FROM Membre m
WHERE membre_numero = 3;

SELECT REF(m) INTO refMembre4
FROM Membre m
WHERE membre_numero = 4;

SELECT REF(m) INTO refMembre5
FROM Membre m
WHERE membre_numero = 5;

SELECT REF(m) INTO refMembre6
FROM Membre m
WHERE membre_numero = 6;

SELECT REF(a) INTO refActivite
FROM Activite a
WHERE activite_numero = 1;

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre, refActivite, 'president');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre2, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre3, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre4, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre5, refActivite, 'membre');

INSERT INTO Responsabilite (membre_numero,activite_numero,resp_membre)
VALUES(refMembre6, refActivite, 'membre');

END;

/

