CREATE OR REPLACE TYPE tMembre AS OBJECT( 
    membre_numero number,
    infos_noms XMLTYPE,
    infos XMLTYPE,
    etat_dossier number(1)
);
/

CREATE TABLE Membre OF tMembre(
    PRIMARY KEY(membre_numero),
    infos_noms NOT NULL, 
    infos NOT NULL,
    etat_dossier NOT NULL,
    CHECK(etat_dossier = 1 OR etat_dossier = 0)
);
/

CREATE OR REPLACE TYPE tActivite AS OBJECT( 
    activite_numero number,
    activite_titre varchar2(30),
    activite_type varchar2(30),
    activite_numAdresse number,
    activite_rueAdresse varchar2(30),
    activite_ville varchar2(30)
);
/

CREATE TABLE Activite OF tActivite(
    PRIMARY KEY(activite_numero),
    activite_titre NOT NULL,
    activite_type NOT NULL,
    activite_numAdresse NOT NULL,
    activite_rueAdresse NOT NULL,
    activite_ville NOT NULL,
    CHECK (activite_type='financement' OR activite_type='competition')
);
/

CREATE OR REPLACE TYPE tResponsabilite AS OBJECT( 
    membre_numero REF tMembre,
    activite_numero REF tActivite,
    resp_membre varchar2(30)
);
/

CREATE TABLE Responsabilite OF tResponsabilite(
    resp_membre NOT NULL,
    membre_numero NOT NULL,
    activite_numero NOT NULL,
    SCOPE FOR(membre_numero) IS Membre,
    SCOPE FOR(activite_numero) IS Activite,
    CHECK (resp_membre = 'president' or resp_membre='membre')
);
/

CREATE OR REPLACE TYPE tDiscipline AS OBJECT ( 
    discipline_numero number,
    discipline_nom varchar2(30)
);
/

CREATE TABLE Discipline OF tDiscipline (
    PRIMARY KEY (discipline_numero),
    discipline_nom NOT NULL
);
/

CREATE OR REPLACE TYPE tSpecialite AS OBJECT (
    spe_numero number, 
    spe_nom varchar2(30), 
    discipline_numero REF tDiscipline
);
/

CREATE TABLE Specialite OF tSpecialite (
    PRIMARY KEY (spe_numero),
    SCOPE FOR(discipline_numero) IS Discipline,
    spe_nom NOT NULL, 
    discipline_numero NOT NULL
);
/

CREATE OR REPLACE TYPE tEpreuve AS OBJECT(
    epreuve_numero number,
    activite_numero REF tActivite, 
    horaire date,
    spe_numero REF tSpecialite 
);
/

CREATE TABLE Epreuve OF tEpreuve(
    PRIMARY KEY(epreuve_numero),
    horaire NOT NULL,
    spe_numero NOT NULL, 
    activite_numero NOT NULL,
    SCOPE FOR(activite_numero) IS Activite,
    SCOPE FOR(spe_numero) IS Specialite
);
/

CREATE OR REPLACE TYPE tResultat AS OBJECT(
    membre_numero REF tMembre, 
    epreuve_numero REF tEpreuve, 
    result_numero number,
    rang number, 
    points number
);
/

CREATE TABLE Resultat OF tResultat(
    PRIMARY KEY(result_numero),
    rang NOT NULL, 
    points NOT NULL, 
    membre_numero NOT NULL, 
    epreuve_numero NOT NULL
);
/

CREATE OR REPLACE TYPE tInscription AS OBJECT (
    insc_numero number, 
    insc_annee number(4), 
    status_membre varchar2(30), 
    insc_frais number,  
    membre_numero REF tMembre
);
/

CREATE TABLE Inscription OF tInscription(
    PRIMARY KEY(insc_numero),
    CHECK(status_membre='am' OR status_membre='pro'),
    CHECK(insc_frais=0 OR insc_frais = 25),
    SCOPE FOR(membre_numero) IS Membre, 
    insc_annee NOT NULL, 
    status_membre NOT NULL, 
    insc_frais NOT NULL, 
    membre_numero NOT NULL
);
/

CREATE OR REPLACE TYPE tPaiement AS OBJECT (
    paiement_numero number,
    paiement_montant number,
    insc_numero REF tInscription
);
/

CREATE TABLE Paiement OF tPaiement(
    PRIMARY KEY(paiement_numero),
    SCOPE FOR (insc_numero) IS Inscription,
    paiement_montant NOT NULL,
    insc_numero NOT NULL
);
/

CREATE TABLE EstSpecialise (
    membre_numero number,
    spe_numero number,
    PRIMARY KEY(membre_numero, spe_numero)
);
/

CREATE OR REPLACE TYPE tFinancement AS OBJECT(
    activite_numero REF tActivite, 
    la_date date
);
/

CREATE TABLE Financement OF tFinancement(
    SCOPE FOR(activite_numero) IS Activite,
    la_date NOT NULL,
    activite_numero NOT NULL 
);
/

CREATE OR REPLACE TYPE tCompetition AS OBJECT(
    activite_numero REF tActivite, 
    date_debut date, 
    date_fin date, 
    lieu varchar2(30), 
    equipes XMLTYPE
);
/

CREATE TABLE Competiton OF tCompetition(
    date_debut NOT NULL,
    date_fin NOT NULL, 
    lieu NOT NULL, 
    activite_numero NOT NULL
);
/

CREATE TABLE CompetitionInterne(
    activite_numero number,
    FOREIGN KEY (activite_numero) REFERENCES Activite(activite_numero)
);
/

CREATE TABLE CompetitionExterne(
    activite_numero number NOT NULL,
    orga_numero number NOT NULL,
    FOREIGN KEY (activite_numero) REFERENCES Activite (activite_numero),
    FOREIGN KEY (orga_numero) REFERENCES OrganisateurExterne(orga_numero)
);
/

CREATE OR REPLACE TYPE tOrganisateurExterne AS OBJECT(
    orga_numero number,
    orga_nom varchar2(30),
    orga_numAdresse number,
    orga_rueAdresse varchar2(30),
    orga_ville varchar2(30),
    nom_responsable varchar2(30),
    prenom_responsable varchar2(30),
    tel_responsable number(10)
);
/

CREATE TABLE OrganisateurExterne OF tOrganisateurExterne (
    PRIMARY KEY(orga_numero),
    orga_nom NOT NULL,
    orga_numAdresse NOT NULL,
    orga_rueAdresse NOT NULL,
    orga_ville NOT NULL,
    nom_responsable NOT NULL,
    prenom_responsable NOT NULL,
    tel_responsable NOT NULL
);
/
