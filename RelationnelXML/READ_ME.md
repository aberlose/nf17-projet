# POC XML

Cette étape du projet consiste à implémenter du XML-relationnel. Le XML permet de résoudre des problèmes que l'on traitait mal en relationnel. Par exemple les tables imbriquées ou les attributs multivalués.

Rappel des consignes :
- Définir deux attributs de type XML pour les membres sachant que le premier regroupe les prénoms et les noms, et le deuxième regroupe les adresses, les numéros de téléphone et les âges des membres sachant que chaque membre doit renseigner deux adresses  et deux numéros de téléphone (domicile et travail respectivement), par contre le renseignement de l’âge est optionnel.

- Définir un attribut XML pour regrouper les noms des joueurs des équipes qui participent aux compétitions.

- Donner les schémas DTD et RelaxNG qui permet de valider les XMLs.

Les fichiers info_prenoms.dtd, info_prenoms.rng et info_prenoms.xml contiennent les schémas et le XML pour l'attribut contenant le nom et le prénom des membres.
Les fichiers adresses.dtd, adresses.rng, adresses.xml contiennent les schémas et le XML pour l'attribut contenant les adresses, téléphones et l'âge.

Les fichiers competition.dtd, competition.rng, competition.xml contiennent les schémas et le XML pour l'attribut contenant les équipes participant aux compétitions.

- Ecrire la requête qui permet d'afficher toutes les adresses des membres qui ont renseigné leurs âges.

La requêtes est située dans le fichier requete.sql.

Les tables sont créées dans le fichier create.sql, les insertions sont faites dans insert.sql et les tables sont supprimées dans le fichier drop.sql.
